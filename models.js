function make(Schema, mongoose, autoIncrement) {

	var trustTransactions = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, rippleName : String
		, addressTrusted : String
		, addressTrustedWith : String
		, value : Number
		, status : String
		, type : String
		, viable : Boolean
		, statusUrl : String
		, transactionFee : Number
		, dateValidatedOut : Date
		, uuid : String
		, currency : String
	});

	trustTransactions.set('autoIndex', false);

	trustTransactions.index({transactionHash : 1, uuid : 1, currency : 1});

	var repeats = new Schema({
		date : Date
		, transactionHash : String
		, dateValidated : Date
		, redeemCode : String
		, neumonicHex : String
		, neumonicWords : String
		, addressTo : String
		, sentDate : Date
		, sent : Boolean
		, introSent : Boolean
		, introSentDate : Date
		, uuid : String
		, addressFrom : String
		, fundingUuid : String
		, fundingStatusUrl : String
		, dateValidatedOut : Date
		, transactionHashOut : String
		, fundingTransactionHashOut : String
		, returnPayment : String
		, returned : Boolean
		, fundingStatus : String
		, statusUrl : String
		, status : String
		, value : Number
		, beforeValue : Number
		, currency : String
		, issuer : String
	});

	repeats.set('autoIndex', false);

	repeats.index({transactionHash : 1});

	var prvCashins = new Schema({
		date : Date
		, dateValidated : Date
		, sentDate : Date
		, sent : Boolean
		, transactionHash : String
		, repeats : [repeats]
		, value : Number
		, currency : String
		, subtype : String
		, dateValidatedOut : Date
		, transactionHashOut : String
		, valueOut : Number
		, accessToken : String
		, otherToken : String
		, socialId : String
		, addressFrom : String
		, redeemCode : String
		, addressTo : String
		, statusUrl : String
		, item : Number
		, return : Boolean
		, status : String
		, returnTransactionHash : String
		, type : String
		, destinationTag : Number
		, roundNumber : Number
		, uuid : String
		, phone : String
		, email : String
		, recipients : String
		, neumonicWords : String
		, neumonicHex : String
		, sendInHash : String
		, address : String
		, name : String
		, rippleName : String
		, message : String
		, from : String
		, snapUser : String
		, snapClient : String
		, explainer : Boolean
		, gateway : { type: Schema.Types.ObjectId, ref: 'Gateway' }
	});

	prvCashins.set('autoIndex', false);

	prvCashins.index({ destinationTag: 1, redeemCode: 1, neumonicWords: 1, neumonicHex: 1, transactionHash: 1, transactionHashOut: 1,  addressTo : 1, addressFrom : 1, type : 1, item : 1});

	var gateways = new Schema({
		date : Date
		, name : String
		, type : String
		, rippleAddress : String
		, rippleName : String
		, domained : String
		, approved : Boolean
		, transactions : [{ type: Schema.Types.ObjectId, ref: 'Escrow' }]
		, currencies : [String]
		, trustTransactions : [trustTransactions]
	});

	gateways.set('autoIndex', false);

	gateways.index({name : 1, type : 1, rippleName : 1, rippleAddress : 1});

	Repeat = mongoose.model('Repeat', repeats),
	Escrow = mongoose.model('Escrow', prvCashins),
	Gateway = mongoose.model('Gateway', gateways),
	TrustTransaction = mongoose.model('TrustTransaction', trustTransactions);

	prvCashins.plugin(autoIncrement.plugin, { model: 'Escrow', field: 'destinationTag' });

	isEmpty = function(ob){
		for(var i in ob){ if(ob.hasOwnProperty(i)){return false;}}
		return true;
	}
}

module.exports.make = make;