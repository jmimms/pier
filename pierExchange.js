var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

if (cluster.isMaster) {

	function eachWorker(callback) {
		for (var id in cluster.workers) {
			callback(cluster.workers[id]);
		}
	}

	function oneWorker() {
		var ids = [];
		for (var id in cluster.workers) {
			ids.push(id);
		}
		var selectedId = ids[Math.floor(Math.random() * ids.length)];;
		return cluster.workers[selectedId];
	}

	var config = require('./config.js')
		, http = require('http')
		, mongoose = require('mongoose')
		, Chance = require('chance')
		, sha256 = require('fast-sha256')
		, ripple = require('ripple-lib')
		, async = require('async')
		, crypto = require('crypto-js')
		, moment = require('moment')
		, mnemonic = require('mnemonic')
		, snapchat = require('snapchat')
		, linkedinClient = require('linkedin-js')(config.inKey, config.inSecret, config.domain)
		, Q = require('q')
		, fb = require('facebook-js')
		, Twitter = require('twitter')
		, bodyParser  = require('body-parser')
		, cronJob = require('cron').CronJob
		, nodemailer = require('nodemailer')
		, FacebookChat = require('facebook-chat')
		, _ = require('underscore')
		, OAuth= require('oauth').OAuth
		, request = require('request')
		, redis = require('redis')
		, client = redis.createClient()
		, qs = require('querystring')
		, session = require('express-session')
		, RedisStore = require('connect-redis')(session)
		, autoIncrement = require('mongoose-auto-increment');

	var lineBalance = 0
		, transactionFee = 0
		, prvLines = []
		, leaderBoard = []
		, cronJobs = [];

	var Remote = ripple.Remote
		, remote = new Remote({
			servers: config.servers
		});

	mongoose.connect(config.mongodb);

	var Schema = mongoose.Schema;
	autoIncrement.initialize(mongoose);

	require('./models.js').make(Schema, mongoose, autoIncrement);

	function sendEmail(toEmail, plainText, html, subject, fromed, name, attachments) {
		var defer = Q.defer();
		var smtpTransport = nodemailer.createTransport("SMTP", {
			host: "localhost",
			port: 25
		});
		var from = name + " <" + fromed + ">";

		var mailOptions = {
			from: from,
			to: toEmail,
			subject: subject,
			text: plainText,
			html: html,
			replyTo: from
		};

		if (attachments){
			mailOptions.atachments = attachments;
		}

		smtpTransport.sendMail(mailOptions, function(error, response){
			smtpTransport.close();
			if ((!error || isEmpty(err))){
				defer.resolve('true');
			}
		});
		return defer.promise;
	}

	function sendLinkedinMessage(token, tokenSecret, people, subject, body){
		var defer = Q.defer();
		var post_data = "<?xml version='1.0' encoding='UTF-8'?><mailbox-item><recipients><recipient><person path=\"/people/" + people + "\" /></recipient></recipients><subject>" + subject + "</subject><body>" + body + "</body></mailbox-item>"

		var oa= new OAuth("https://api.linkedin.com/uas/oauth/requestToken",
			"https://api.linkedin.com/uas/oauth/accessToken",
			config.inKey, config.inSecret, "1.0", "http://pier.exchange/linkedin", "HMAC-SHA1");
		oa.post("http://api.linkedin.com/v1/people/~/mailbox", token, tokenSecret, post_data, 'text/xml', function(error, data){
			if (!error){
				defer.resolve();
			}
		});
		return defer.promise;
	}

	function postLinkedinUpdate(token, tokenSecret, packet){
		var defer = Q.defer();

		var title = packet.value + ' ' + packet.currency + " from " + packet.gateway + " up for grabs!";

		var post_data = "<?xml version='1.0' encoding='UTF-8'?><share><comment>" + title + "</comment><content><title>" + title + "</title><description>" + "Redeem at Pier Exchange" + "</description><submitted-url>https://beta.pier.exchange/#/' + encodeURIComponent(packet.redeemCode) + '</submitted-url><submitted-image-url>https://d1pzmogk9nquph.cloudfront.net/img/pierCircle.png</submitted-image-url></content><visibility><code>anyone</code></visibility></share>"

		var oa= new OAuth("https://api.linkedin.com/uas/oauth/requestToken",
			"https://api.linkedin.com/uas/oauth/accessToken",
			config.inKey, config.inSecret, "1.0", "http://pier.exchange/linkedin", "HMAC-SHA1");
		oa.post("http://api.linkedin.com/v1/people/~/mailbox", token, tokenSecret, post_data, 'text/xml', function(error, data){
			if (!error){
				defer.resolve();
			}
		});
		return defer.promise;
	}

	function sendFacebookMessage(token, tokenSecret, facebookId, people, body){
		var defer = Q.defer();
		var params = {
			facebookId : facebookId,
			appId :  config.fbClientKey,
			appSecret : config.fbClientSecret,
			accessToken : token
		};

		var facebookClient = new FacebookChat(params);

		facebookClient.on('online', function(){

			facebookClient.send('-' + people + '@chat.facebook.com', body);

			facebookClient.disconnect();

		});

		return defer.promise;
	}

	function postFacebookUpdate(token, tokenSecret, packet){
		var defer = Q.defer();
		fb.apiCall('POST', '/me/feed',
			{access_token: token, message: packet.value + ' ' + packet.currency + ' from ' + packet.gateway + ' redeemable at ' + 'https://beta.pier.exchange/#/' + encodeURIComponent(packet.redeemCode)},
			function (error, response, body) {
				if (!error){
					defer.resolve();
				}
			}
		);
		return defer.promise;
	}

	function sendTwitterMessage(token, tokenSecret, people, body){
		var defer = Q.defer();
		var client = new Twitter({
			consumer_key: config.twitterKey,
			consumer_secret: config.twitterSecret,
			access_token_key: token,
			access_token_secret: tokenSecret
		});
		client.newDirectMessage(people, body, function(error, response){
			if (!error){
				defer.resolve();
			}
		});
		return defer.promise;
	}

	function postTwitterUpdate(token, tokenSecret, body){
		var defer = Q.defer();
		var client = new Twitter({
			consumer_key: config.twitterKey,
			consumer_secret: config.twitterSecret,
			access_token_key: token,
			access_token_secret: tokenSecret
		});
		client.post('statuses/update', {status: body},  function(error, response){
			if (!error){
				defer.resolve();
			}
		});
		return defer.promise;
	}

	function sendMoneyMail(toEmail, fromed, name, attachments, amount, currency, message, year, url, gatewayName, gatewayDomain){
		var deferred = Q.defer();
		var html = '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Pier</title> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; }@media only screen and (max-width: 640px){.header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr> <td height="30"></td> </tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="133c66"> <td height="5"></td> </tr> <tr bgcolor="133c66"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td width="30" height="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="133c66"> <td height="10"></td> </tr> </table> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <tr bgcolor="ececec"> <td height="40"></td> </tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://beta.pier.exchange" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://d1pzmogk9nquph.cloudfront.net/email/pier-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="10"></td> </tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #ffffff" href="https://beta.pier.exchange" target="_blank">Home</a> <span class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #ffffff" href="https://beta.pier.exchange" target="_blank">Pier.Exchange</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"> <td height="40"></td> </tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://d1pzmogk9nquph.cloudfront.net/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td> </tr> <tr bgcolor="ffffff"> <td height="7"></td> </tr> <tr bgcolor="ffffff"> <td align="center"><a style="text-decoration: none; color: #ffffff" href="' + url + '" target="_blank"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://d1pzmogk9nquph.cloudfront.net/email/pierEmail.png" alt="large image" class="header-bg" /></a></td> </tr> <tr bgcolor="ffffff"> <td height="20"></td> </tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #133c66; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline>Sent ' + amount + ' ' + currency + ' issued by <a href="' + gatewayDomain + '" target="_blank">' + gatewayName + '</a> over the Ripple protocol. <a style="text-decoration: none;" href="' + url + '" target="_blank">Collect here</a>.</multiline> </td> </tr> <tr> <td height="20"></td> </tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline>' + message + '</multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"> <td height="25"></td> </tr> <tr> <td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://d1pzmogk9nquph.cloudfront.net/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td> </tr> </table> </td> </tr> <tr> <td height="35"></td> </tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://beta.pier.exchange" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/pier-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr> <td height="10"></td> </tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/PierExchange" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/peercover" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr> <td height="30"></td> </tr> </table> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="133c66"> <td height="14"></td> </tr> <tr bgcolor="133c66"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Pier Inc. © Copyright ' + year + '. All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> </td> </tr> <tr> <td height="30"></td> </tr> </table> </body> </html>';
		sendEmail(toEmail, amount + ' ' + currency + ' from ' + name, html, amount + ' ' + currency + ' from ' + name, fromed, name, attachments).then(function(){
			deferred.resolve(true);
		});
		return deferred.promise;
	}

	function hex2a(hex) {
		var str = '';
		for (var i = 0; i < hex.length; i += 2)
			str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		return str;
	}
	function encryptToDb (input){
		return crypto.AES.encrypt(input, config.cryptoPhrase).toString();
	}
	function decryptFromDb (input){
		return hex2a(crypto.AES.decrypt(input, config.cryptoPhrase).toString());
	}

	function settedDate(pr, todayDate, nextDate){
		if (pr.recurringSeconds){
			nextDate.setSeconds(todayDate.getSeconds() + pr.recurringSeconds);
		}
		if (pr.recurringHours){
			nextDate.setHours(todayDate.getHours() + pr.recurringHours);
		}
		if (pr.recurringDays){
			nextDate.setDate(todayDate.getDate() + pr.recurringDays);
		}
		if (pr.recurringMonths){
			nextDate.setMonth(todayDate.getMonth() + pr.recurringMonths);
		}
		if (pr.recurringYears){
			nextDate.setYear(todayDate.getYear() + pr.recurringYears);
		}
		return nextDate;
	}

	function jobbed(pr){
		var job = new cronJob(pr.endDate, function(){
				setTimeout(function(){
					paymentsInit(pr.endDate).then(function(){

					}).then(function(lastLedger){
							if (lastLedger && lastLedger[0]){
								client.set('ledgerIndexPier', lastLedger[0]);
							}
						});
				}, 4000);
			}, function () {
			},
			true,
			'UTC'
		);
		cronJobs.push({job : job, _id : pr._id});
	}

	function initJob(pr){
		if (pr.ended && (pr.recurring && (pr.recurringSeconds || pr.recurringHours || pr.recurringDays || pr.recurringMonths || pr.recurringYears))){
			pr.ended = undefined;
			var todayDate = new Date();
			var nextDate = new Date();
			pr.endDate = settedDate(pr, todayDate, nextDate);
			pr.save(function(err,pr){
				if (!isEmpty(pr)){
					jobbed(pr);
				}
			});
		}
		else if (!pr.ended){
			jobbed(pr);
		}
	}

	function returnPayment(transhash, repeat, payment){
		if (!repeat.uuid && !repeat.fundingUuid){
			repeat.returned = true;
			transhash.save(function(err,tran){
				if (!isEmpty(tran)){
					var repeated = tran.repeats.id(repeat._id);
					request(config.rippleRest + '/v1/uuid', function (error, response, body) {
						if (!error && response.statusCode == 200) {
							if (body.success){
								repeated.uuid = body.uuid;
								transhash.save(function(err,finCash){
									if (isEmpty(err) && !isEmpty(finCash)){
										var returne = finCash.returns.id(repeat._id);
										request(config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments/paths/' + repeat.addressFrom + '/' + repeat.value.toString() + '+' + repeat.currency + '+' + repeat.issuer +  '?source_currencies=' + repeat.currency + '+' + repeat.issuer, function (error, response, body) {
											if (typeof body == 'string'){
												body = JSON.parse(body);
											}
											if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {
												var formData = {secret : config.secretKey, client_resource_id : finCash.uuid, payment : body.payments[0]};
												request.post({headers: {'content-type' : 'application/json'}, body: JSON.stringify(formData), url: config.rippleRest + '/v1/accounts/' + payment.source_account + '/payments?validated=true'}, function optionalCallback(err, httpResponse, body) {
													if (typeof body == 'string'){
														body = JSON.parse(body);
													}
													if (!error && response.statusCode == 200 && body.success) {

														returne.statusUrl = body.status_url;

														finCash.save(function(err, doned){

														});
													}
													else {
														returne.uuid = '';

														finCash.save(function(err, doned){

														});
													}
												});
											}
											else {
												returne.uuid = '';

												finCash.save(function(err, doned){

												});
											}
										});
									}
								});
							}
						}
					});
				}
			});
		}
	}

	function releaseEscrow(escrow, repeat, address){
		var defer = Q.defer();
		if (!escrow.returned){
			request(config.rippleRest + '/v1/transactions/' + decryptFromDb(escrow.transactionHash), function (error, response, body) {
				if (!error && response.statusCode == 200 && body.success) {
					request(config.rippleRest + '/v1/uuid', function (error, response, body) {
						if (!error && response.statusCode == 200 && body.success) {
							repeat.uuid = body.uuid;
							repeat.addressTo = address;
							escrow.save(function(err,finCash){
								if (isEmpty(err)){
									var rippleFunc = function(){
										var request = Remote.requestAccountInfo({account : 'raao4egwGfCNWxCUg1jqzzZL5BY9Rzrre9'}, function(err, info) {
											if (err && err.remote && err.remote.err == 'actNotFound'){
												repeat.uuid = '';
												repeat.fundingUuid = body.uuid;
												if ((((!isNaN(Number(body.transaction.Amount)) && Number(body.transaction.Amount) <= repeat.value) ||  (repeat.value <= body.transaction.Amount.value))) && (repeat.currency == body.transaction.Amount.currency) && ((!repeat.issuer && !body.transaction.Amount.issuer) || (repeat.issuer == body.transaction.Amount.issuer))){
													var request = Remote.requestServerInfo();
													request.request(function(err, res) {
														if (res && res.info && res.info.validated_ledger && res.info.validated_ledger.reserve_base_xrp && res.info.validated_ledger.reserve_inc_xrp){
															var reserve = Number(res.info.validated_ledger.reserve_base_xrp) + Number(res.info.validated_ledger.reserve_inc_xrp) + 1;
															request(config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments/paths/' + address + '/' + reserve.toString() + '+' + 'XRP' + '?source_currencies=' + repeat.currency + '+' + repeat.issuer, function (error, response, body) {
																if (typeof body == 'string'){
																	body = JSON.parse(body);
																}
																if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {

																	var finalPayment;

																	body.payments.forEach(function(payment){
																		if (payment.source_amount && payment.source_amount.currency == repeat.currency && repeat.value >= Number(payment.source_amount.value)){
																			if (finalPayment && Number(finalPayment.source_amount.value) > Number(payment.source_amount.value)){
																				finalPayment = payment;
																			}
																			else {
																				finalPayment = payment;
																			}
																		}
																	});

																	if (finalPayment){
																		var formData = {secret : config.secretKey, client_resource_id : finCash.fundingUuid, payment : finalPayment};

																		formData.payment.memos = [{MemoType : 'Escrow Release', MemoData : JSON.stringify({hashReference : escrow.transactionHash})}];
																		formData.payment.source_tag = finCash.destination_tag;

																		request.post({headers: {'content-type' : 'application/json'}, body: JSON.stringify(formData), url: config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments?validated=true'}, function optionalCallback(err, httpResponse, body) {
																			if (typeof body == 'string'){
																				body = JSON.parse(body);
																			}
																			if (!error && response.statusCode == 200 && body.success) {
																				if (!repeat.beforeValue){
																					repeat.beforeValue = repeat.value;
																				}
																				repeat.value = repeat.beforeValue - Number(payment.source_amount.value);
																				repeat.fundingStatusUrl = body.status_url;
																				finCash.save(function(err, doned){

																				});
																			}
																			else {
																				repeat.fundingUuid = '';
																				finCash.save(function(err, doned){

																				});
																			}
																		});
																	}
																}
																else {
																	repeat.fundingUuid = '';
																	finCash.save(function(err, doned){

																	});
																}
															});
														}
													});
												}
											}
											else if (!err && info){
												if ((((!isNaN(Number(body.transaction.Amount)) && Number(body.transaction.Amount) <= repeat.value) ||  (repeat.value <= body.transaction.Amount.value))) && (repeat.currency == body.transaction.Amount.currency) && ((!repeat.issuer && !body.transaction.Amount.issuer) || (repeat.issuer == body.transaction.Amount.issuer))){
													request(config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments/paths/' + address + '/' + (Number(repeat.value) * 0.005).toFixed(6) + '+' + repeat.currency + '+' + repeat.issuer +  '?source_currencies=' + repeat.currency + '+' + repeat.issuer, function (error, response, body) {
														if (typeof body == 'string'){
															body = JSON.parse(body);
														}
														if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {
															var formData = {secret : config.secretKey, client_resource_id : finCash.uuid, payment : body.payments[0]};

															formData.payment.memos = [{MemoType : 'Escrow Release', MemoData : JSON.stringify({hashReference : escrow.transactionHash})}];
															formData.payment.source_tag = finCash.destination_tag;

															request.post({headers: {'content-type' : 'application/json'}, body: JSON.stringify(formData), url: config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments?validated=true'}, function optionalCallback(err, httpResponse, body) {
																if (typeof body == 'string'){
																	body = JSON.parse(body);
																}
																if (!error && response.statusCode == 200 && body.success) {
																	repeat.statusUrl = body.status_url;
																	finCash.save(function(err, doned){

																	});
																}
																else {
																	repeat.uuid = '';
																	finCash.save(function(err, doned){

																	});
																}
															});
														}
														else {
															repeat.uuid = '';
															finCash.save(function(err, doned){

															});
														}
													});
												}
											}
										});

									}
									if (Remote.isConnected()){
										rippleFunc();
									}
									else {
										Remote.connect(function() {
											rippleFunc();
										});
									}
								}
							});
						}
					});
				}
			});
		}
		return defer.promise;
	}

	function escrowChecker(){
		Escrow.find({$or: [
			{
				$or: [
					{'repeats.fundingUuid': {$exists: false}},
					{'repeats.uuid': {$exists: false}}
				]
			},
			{
				$and: [
					{'repeats.fundingUuid': {$exists: true}},
					{'repeats.uuid': {$exists: true}},
					{$or: [
						{'repeats.status': {$ne: 'success'}},
						{'repeats.fundingStatus': {$ne: 'success'}}
					]}
				]
			},
			{
				$and: [
					{'repeats.uuid': {$exists: true}},
					{'repeats.status': {$ne: 'success'}}
				]
			}
		]}, function(err,escrow){
			if (!err && !isEmpty(escrow) && escrow.repeats && escrow.repeats[0]){
				escrow.repeats.forEach(function(repeat){
					if (repeat.fundingUuid && !repeat.uuid && repeat.fundingStatus != 'success' && repeat.fundingStatusUrl){
						request(config.rippleRest + repeat.fundingStatusUrl, function (error, response, body) {
							if (typeof body == 'string'){
								body = JSON.parse(body);
							}
							if (!error && response.statusCode == 200 && body.success) {
								if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
									repeat.fundingStatus = 'success';
									repeat.fundingTransactionHashOut = body.payment.hash;
									escrow.save(function(err, finalCashin){

									});
								}
								else if (body.payment.state == 'validated'){
									releaseEscrow(escrow, repeat, repeat.addressTo);
								}
							}
						});
					}
					else if (repeat.uuid && repeat.status !== 'success' && repeat.statusUrl){
						if (!repeat.returned){
							request(config.rippleRest + repeat.statusUrl, function (error, response, body) {
								if (typeof body == 'string'){
									body = JSON.parse(body);
								}
								if (!error && response.statusCode == 200 && body.success) {
									if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
										repeat.status = 'success';
										repeat.transactionHashOut = body.payment.hash;
										escrow.save(function(err, finalCashin){

										});
									}
									else if (body.payment.state == 'validated'){
										releaseEscrow(escrow, repeat, repeat.addressTo);
									}
								}
							});
						}
						else {
							returnPayment(escrow, repeat, JSON.parse(decryptFromDb(repeat.returnPayment)));
						}
					}
				});
			}
		});
	}

	// TODO : should simply send over socket transactionID and initiate social functions client side without storing keys

	// TODO : another production branch utilizing browserify

	function paymentConditionals(payment){
		var tad = Q.defer();
		if (!isNaN(Number(payment.destination_tag)) && payment.destination_amount){
			Escrow.findOne({destinationTag : payment.destination_tag}, function(err, escrow){
				if (!err && !isEmpty(escrow)){
					var alreadyIn;
					if (escrow.repeats){
						escrow.repeats.forEach(function(repeat){
							if (decryptFromDb(repeat.transactionHash) == payment.hash){
								alreadyIn = true;
							}
						});
					}
					if (!alreadyIn){
						var repeat = new Repeat({transactionHash: encryptToDb(payment.hash), addressFrom : payment.source_account});
						if (isNaN(payment.destination_amount)){
							repeat.value = payment.destination_amount.value;
							repeat.currency = payment.destination_amount.currency;
							repeat.issuer = payment.destination_amount.issuer;
						}
						else {
							repeat.value = payment.destination_amount;
							repeat.currency = 'XRP';
						}
						escrow.repeats.push(repeat);
						if (escrow.type == 'snapchat'){
							escrow.save(function(err, transhash){
								if (!isEmpty(transhash)){
									var time = 6;
									var repeated = transhash.repeats.id(repeat._id);
									var wordArray = [];
									var wordHex;
									var wordString = '';
									for (var i = 0; i < 7; i++){
										wordArray.push(mnemonic.words[Math.floor(Math.random()*mnemonic.words.length)]);
										if (i == 6){
											wordString += wordArray[i] + ' ';
										}
										else {
											wordString += wordArray[i];
										}
									}
									wordHex = mnemonic.decode(wordArray);
									var words = mnemonic.words
									gm("/home/ubuntu/pier/www/img/snapTemplate.jpg")
										.font("/home/ubuntu/pier/www/fonts/neon-webfont.ttf", 12)
										.fill("#6acdec")
										.drawText(100, 300, transhash.words)
										.write("/home/ubuntu/tempImages/" + transhash.neumonicHex + ".jpg", function (err) {
											if (!err){
												var c = new snapchat.Client();
												if (transhash.explainer){
													c.login(decryptFromDb(transhash.snapUser), decryptFromDb(transhash.snapClient))
														.then(function() {
															repeated.neumonicHex = encryptToDb(wordHex);
															repeated.neumonicWords = encryptToDb(wordString);
															transhash.save(function(err,tran){

															});
															var blob = fs.createReadStream("/home/ubuntu/pier/www/video/pierExplainer.mp4");
															return c.upload(blob, true);
														}, function(err) {
															repeated.returnPayment = encryptToDb(JSON.stringify(payment));
															transhash.save(function(err,escrow){
																if (!isEmpty(escrow)){
																	returnPayment(escrow, escrow.repeats.id(repeated._id), payment);
																}
															});
														})
														.then(function(mediaId) {
															return Q.allSettled(transhash.recipients.split(' ').map(function(recipient) {
																return c.send(mediaId, recipient).catch(function(err) {
																});
															}));
														}, function(error) {
															repeated.returnPayment = encryptToDb(JSON.stringify(payment));
															transhash.save(function(err,escrow){
																if (!isEmpty(escrow)){
																	returnPayment(escrow, escrow.repeats.id(repeated._id), payment);
																}
															});
														})
														.then(function(statuses) {
															Escrow.findOne({destinationTag : payment.destination_tag}, function(err, escrow){
																if (!isEmpty(escrow)){
																	var repeated = escrow.repeats.id(repeat._id);
																	repeated.intoSent = true;
																	repeated.introSentDate = new Date();
																	var blob = fs.createReadStream("/home/ubuntu/tempImages/" + repeated.neumonicHex + ".jpg");
																	return c.upload(blob, true);
																}
															});
														}, function(err) {
															repeated.returnPayment = encryptToDb(JSON.stringify(payment));
															transhash.save(function(err,escrow){
																if (!isEmpty(escrow)){
																	returnPayment(escrow, escrow.repeats.id(repeated._id), payment);
																}
															});
														})
														.then(function(mediaId) {
															return Q.allSettled(transhash.recipients.split(' ').map(function(recipient) {
																return c.send(mediaId, recipient, time).catch(function(err) {

																});
															}));
														}, function(err) {

														})
														.then(function(statuses) {
															Escrow.findOne({destinationTag : payment.destination_tag}, function(err, escrow){
																if (!isEmpty(escrow)){
																	var repeated = escrow.repeats.id(repeat._id);
																	repeated.sent = true;
																	repeated.sentDate = new Date();
																	fs.unlinkSync("/home/ubuntu/tempImages/" + repeated.neumonicHex + ".jpg");
																}
															});
														}, function(err) {

														});
												}
												else {
													c.login(decryptFromDb(transhash.snapUser), decryptFromDb(transhash.snapClient))
														.then(function(statuses) {
															var blob = fs.createReadStream("/home/ubuntu/tempImages/" + transhash.neumonicHex + ".jpg");
															repeated.neumonicHex = encryptToDb(wordHex);
															repeated.neumonicWords = encryptToDb(wordString);
															transhash.save(function(err,tran){

															});
															return c.upload(blob, true);
														}, function(err) {
															repeated.returnPayment = encryptToDb(JSON.stringify(payment));
															transhash.save(function(err,escrow){
																if (!isEmpty(escrow)){
																	returnPayment(escrow, escrow.repeats.id(repeated._id), payment);
																}
															});
														})
														.then(function(mediaId) {
															return Q.allSettled(transhash.recipients.split(' ').map(function(recipient) {
																return c.send(mediaId, recipient, time).catch(function(err) {

																});
															}));
														}, function(err) {

														})
														.then(function(statuses) {
															Escrow.findOne({destinationTag : payment.destination_tag}, function(err, escrow){
																if (!isEmpty(escrow)){
																	var repeated = escrow.repeats.id(repeat._id);
																	repeated.sent = true;
																	repeated.sentDate = new Date();
																	fs.unlinkSync("/home/ubuntu/tempImages/" + repeated.neumonicHex + ".jpg");
																}
															});
														}, function(err) {

														});
												}
											}
										});
								}
							});
						}
						else if (escrow.type == 'email'){
							Gateway.findOne({rippleAddress : payment.destination_amount.issuer}, function(err, gated){
								if (!isEmpty(gated)){
									var recoverCode = crypto.lib.WordArray.random(64);
									recoverCode = crypto.enc.Base64.stringify(recoverCode);
									repeat.redeemCode = encryptToDb(recoverCode);
									escrow.save(function(err,escrow){
										if (!isEmpty(escrow)){
											var repeated = escrow.repeats.id(repeat._id);
											function sendNow(){
												var deferred = [];
												var recipients = escrow.recipients.split(' ');
												recipients.forEach(function(recipient){
													var def = Q.defer();
													deferred.push(def.promise);
													var date = new Date();
													var year = date.getFullYear();
													sendMoneyMail(recipient, escrow.email, escrow.name || escrow.rippleName, '', payment.destination_amount.value, payment.destination_amount.currency, escrow.message, year, 'https://beta.pier.exchange/#/' + encodeURIComponent(recoverCode), gated.name, gated.domained)
												});
												return Q.all(deferred);
											}
											sendNow().then(function(){
												repeated.sent = true;
												repeated.sentDate = new Date();
												escrow.save(function(err,fine){

												});
											}, function(err){
												repeated.returnPayment = encryptToDb(JSON.stringify(payment));
												escrow.save(function(err,escrow){
													if (!isEmpty(escrow)){
														returnPayment(escrow, escrow.repeats.id(repeated._id), payment);
													}
												});
											});
										}
									});
								}
							});
						}
						else if (escrow.type == 'sms'){
							Gateway.findOne({rippleAddress : payment.destination_amount.issuer}, function(err, gated){
								if (!isEmpty(gated)){
									var recoverCode = crypto.lib.WordArray.random(64);
									recoverCode = crypto.enc.Base64.stringify(recoverCode);
									repeat.redeemCode = encryptToDb(recoverCode);
									escrow.save(function(err,escrow){
										if (!isEmpty(escrow)){
											var repeated = escrow.repeats.id(repeat._id);
											function sendNow(){
												var deferred = [];
												var recipients = escrow.recipients.split(' ');
												recipients.forEach(function(recipient){
													var def = Q.defer();
													deferred.push(def.promise);
													request.post({headers: {'content-type': 'application/json'}, url: 'https://www.paksmsgh.com/components/com_smsreseller/smsapi.php?username=' + qs.stringify(config.smsUsername) + '&password=' + qs.stringify(config.smsPassword) + '&sender=' + qs.stringify(escrow.from) + '&message=' + qs.stringify(escrow.message + ' ' + recoverCode + ' ' + 'https://beta.pier.exchange/#/' + encodeURIComponent(recoverCode))}, function(error, response, body) {
														if (!error && response.statusCode == 200 ) {
															def.resolve();
														}
													});

												});
												return Q.all(deferred);
											}
											sendNow().then(function(){
												repeated.sent = true;
												repeated.sentDate = new Date();
												escrow.save(function(err,fine){

												});
											}, function(err){
												escrow.save(function(err,escrow){
													if (!isEmpty(escrow)){

													}
												});
											});
										}
									});
								}
							});
						}
						else if (escrow.type == 'facebook'){
							Gateway.findOne({rippleAddress : payment.destination_amount.issuer}, function(err, gated){
								if (!isEmpty(gated)){
									var recoverCode = crypto.lib.WordArray.random(64);
									recoverCode = crypto.enc.Base64.stringify(recoverCode);
									repeat.redeemCode = encryptToDb(recoverCode);
									escrow.save(function(err,escrow){
										if (!isEmpty(escrow)){
											var repeated = escrow.repeats.id(repeat._id);
											if (escrow.subtype == 'dm'){
												function deferReturn(){
													var deferred = [];
													escrow.recipients.split(' ').forEach(function(es){
														var defer = Q.defer();
														deferred.push(defer.promise);
														var packet = {};
														packet.gateway = gated.name;
														packet.currency = repeated.currency;
														packet.value = repeated.value;
														packet.issuer = repeated.issuer;
														packet.redeemCode = repeated.redeemCode;
														sendFacebookMessage(decryptFromDb(escrow.accessToken), decryptFromDb(escrow.otherToken), escrow.socialId, es, 'You received ' + repeated.value + ' ' + repeated.currency + ' over Ripple. Redeem ' + repeated.value + ' ' + repeated.currency + ' from ' + gated.name + ' over Pier.' + ' https://beta.pier.exchange/' + encodeURIComponent(repeated.redeemCode)).then(function(){
															defer.resolve();
														});
													});
													return Q.all(deferred);
												}
												deferReturn().then(function(){
													repeated.sent = true;
													repeated.sentDate = new Date();
													escrow.save(function(err,fine){

													});
												}, function(err){
													repeated.returnPayment = encryptToDb(JSON.stringify(payment));
													escrow.save(function(err,escrow){
														if (!isEmpty(escrow)){

														}
													});
												});
											}
											else if (escrow.subtype == 'upForGrabs'){
												function deferReturn(){
													var defer = Q.defer();
													var packet = {};
													packet.gateway = gated.name;
													packet.currency = repeated.currency;
													packet.value = repeated.value;
													packet.issuer = repeated.issuer;
													packet.redeemCode = repeated.redeemCode;
													postFacebookUpdate(decryptFromDb(escrow.accessToken), decryptFromDb(escrow.otherToken), packet).then(function(){
														defer.resolve();
													});
													return defer.promise;
												}
												deferReturn().then(function(){
													repeated.sent = true;
													repeated.sentDate = new Date();
													escrow.save(function(err,fine){

													});
												}, function(err){
													repeated.returnPayment = encryptToDb(JSON.stringify(payment));
													escrow.save(function(err,escrow){
														if (!isEmpty(escrow)){

														}
													});
												});
											}
										}
									});
								}
							});
						}
						else if (escrow.type == 'linkedin'){
							Gateway.findOne({rippleAddress : payment.destination_amount.issuer}, function(err, gated){
								if (!isEmpty(gated)){
									var recoverCode = crypto.lib.WordArray.random(64);
									recoverCode = crypto.enc.Base64.stringify(recoverCode);
									repeat.redeemCode = encryptToDb(recoverCode);
									escrow.save(function(err,escrow){
										if (!isEmpty(escrow)){
											var repeated = escrow.repeats.id(repeat._id);
											if (escrow.subtype == 'dm'){
												function deferReturn(){
													var deferred = [];
													escrow.recipients.split(' ').forEach(function(es){
														var defer = Q.defer();
														deferred.push(defer.promise);
														var packet = {};
														packet.gateway = gated.name;
														packet.currency = repeated.currency;
														packet.value = repeated.value;
														packet.issuer = repeated.issuer;
														packet.redeemCode = repeated.redeemCode;
														sendLinkedinMessage(decryptFromDb(escrow.accessToken), decryptFromDb(escrow.otherToken), es, 'You received ' + repeated.value + ' ' + repeated.currency + ' over Ripple.', 'Redeem ' + repeated.value + ' ' + repeated.currency + ' from ' + gated.name + ' over Pier.' + ' https://beta.pier.exchange/' + encodeURIComponent(repeated.redeemCode)).then(function(){
															defer.resolve();
														});
													});
													return Q.all(deferred);
												}
												deferReturn().then(function(){
													repeated.sent = true;
													repeated.sentDate = new Date();
													escrow.save(function(err,fine){

													});
												}, function(err){
													repeated.returnPayment = encryptToDb(JSON.stringify(payment));
													escrow.save(function(err,escrow){
														if (!isEmpty(escrow)){

														}
													});
												});
											}
											else if (escrow.subtype == 'upForGrabs'){
												function deferReturn(){
													var defer = Q.defer();
													var packet = {};
													packet.gateway = gated.name;
													packet.currency = repeated.currency;
													packet.value = repeated.value;
													packet.issuer = repeated.issuer;
													packet.redeemCode = repeated.redeemCode;
													postLinkedinUpdate(decryptFromDb(escrow.accessToken), decryptFromDb(escrow.otherToken), es, packet).then(function(){
														defer.resolve();
													});
													return defer.promise;
												}
												deferReturn().then(function(){
													repeated.sent = true;
													repeated.sentDate = new Date();
													escrow.save(function(err,fine){

													});
												}, function(err){
													repeated.returnPayment = encryptToDb(JSON.stringify(payment));
													escrow.save(function(err,escrow){
														if (!isEmpty(escrow)){

														}
													});
												});
											}
										}
									});
								}
							});
						}
						else if (escrow.type == 'twitter'){
							Gateway.findOne({rippleAddress : payment.destination_amount.issuer}, function(err, gated){
								if (!isEmpty(gated)){
									var recoverCode = crypto.lib.WordArray.random(64);
									recoverCode = crypto.enc.Base64.stringify(recoverCode);
									repeat.redeemCode = encryptToDb(recoverCode);
									escrow.save(function(err,escrow){
										if (!isEmpty(escrow)){
											var repeated = escrow.repeats.id(repeat._id);
											if (escrow.subtype == 'dm'){
												function deferReturn(){
													var deferred = [];
													escrow.recipients.split(' ').forEach(function(es){
														var defer = Q.defer();
														deferred.push(defer.promise);
														var packet = {};
														packet.gateway = gated.name;
														packet.currency = repeated.currency;
														packet.value = repeated.value;
														packet.issuer = repeated.issuer;
														packet.redeemCode = repeated.redeemCode;
														sendTwitterMessage(decryptFromDb(escrow.accessToken), decryptFromDb(escrow.otherToken), es, 'Redeem ' + repeated.value + ' ' + repeated.currency + ' from ' + gated.name + ' over Pier.' + ' https://beta.pier.exchange/#/' + encodeURIComponent(repeated.redeemCode) + ' #PierExchange').then(function(){
															defer.resolve();
														});
													});
													return Q.all(deferred);
												}
												deferReturn().then(function(){
													repeated.sent = true;
													repeated.sentDate = new Date();
													escrow.save(function(err,fine){

													});
												}, function(err){
													repeated.returnPayment = encryptToDb(JSON.stringify(payment));
													escrow.save(function(err,escrow){
														if (!isEmpty(escrow)){

														}
													});
												});
											}
											else if (escrow.subtype == 'upForGrabs'){
												function deferReturn(){
													var defer = Q.defer();
													var packet = {};
													packet.gateway = gated.name;
													packet.currency = repeated.currency;
													packet.value = repeated.value;
													packet.issuer = repeated.issuer;
													packet.redeemCode = repeated.redeemCode;
													postTwitterUpdate(decryptFromDb(escrow.accessToken), decryptFromDb(escrow.otherToken), 'Redeem ' + repeated.value + ' ' + repeated.currency + ' from ' + gated.name + ' over Pier.' + ' https://beta.pier.exchange/#/' + encodeURIComponent(repeated.redeemCode) + ' #PierExchange').then(function(){
														defer.resolve();
													});
													return defer.promise;
												}
												deferReturn().then(function(){
													repeated.sent = true;
													repeated.sentDate = new Date();
													escrow.save(function(err,fine){

													});
												}, function(err){
													repeated.returnPayment = encryptToDb(JSON.stringify(payment));
													escrow.save(function(err,escrow){
														if (!isEmpty(escrow)){

														}
													});
												});
											}
										}
									});
								}
							});
						}
					}
				}
			});
			tad.resolve('');
		}
		return tad.promise;
	}

	function getFees(){
		request(config.rippleRest + '/v1/server', function (error, response, body) {
			if (typeof body == 'string'){
				body = JSON.parse(body);
			}
			if (!error && response.statusCode == 200 && body.success && body.rippled_server_status && body.validated_ledger) {
				transactionFee = Number(body.validated_ledger.base_fee_xrp) * Number(body.rippled_server_status.load_factor) / 256;
			}
		});
	}

	function paymentsInit(date){
		var page = 1
		, lastLedger
		, deferArray = []
		, ta = Q.defer();
		function recursivePages(page, index){
			var url = config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments?destination_account=' + config.publicKey + '&exclude_failed=true&direction=incoming&page=' + page.toString();
			if (index){
				url += '&start_ledger=' + index;
			}
			request(url, function (error, response, body) {
				if (typeof body == 'string'){
					body = JSON.parse(body);
				}
				if (!error && response.statusCode == 200 && body && body.success && body.payments && body.payments[0]) {
					body.payments.forEach(function(payment){
						paymentConditionals(payment).then(function(){

						});
					});
					lastLedger = body.payments[0].ledger;
					if (((date && new Date(body.payments[body.payments.length - 1].timestamp) < date) || !date) && body.payments.length == 10){
						page += 1;
						recursivePages(page, index);
					}
					else {
						if (lastLedger){
							ta.resolve(lastLedger.toString());
						}
					}
				}
				else if (body && body.success){
					if (lastLedger){
						ta.resolve(lastLedger.toString());
					}
				}
			});
		}
		client.get('ledgerIndexPier', function(err, ressed){
			recursivePages(page, ressed);
		});
		return ta.promise;
	}

	function rippleSubscribe(){
		var rippleFunc = function(){
			var request = remote.requestSubscribe(['transactions']);
			request.setServer([ 'wss://s1.ripple.com:443' ]);
			remote.on('transaction', function onTransaction(transaction) {
				if (transaction.engine_result == 'tesSUCCESS' && transaction.type == 'transaction' && transaction.transaction.TransactionType == 'Payment' && transaction.transaction.Destination == config.publicKey){
					var payment = {};
					payment.source_account = transaction.transaction.Account;
					payment.destination_amount = {};
					if (!isNaN(Number(transaction.transaction.Amount))){
						payment.destination_amount.currency = 'XRP';
						payment.destination_amount.value = transaction.transaction.Amount;
					}
					else {
						payment.destination_amount = transaction.transaction.Amount;
					}
					payment.hash = transaction.transaction.hash;
					payment.memos = transaction.transaction.Memos;
					paymentConditionals(payment);
				}
			});
			request.request();
		}
		if (remote.isConnected()){
			rippleFunc();
		}
		else {
			remote.connect(function() {
				rippleFunc();
			});
		}
	}

	function sendTrust(gated, transact){
		request(config.rippleRest + '/v1/uuid', function (error, response, body) {
			if (typeof body == 'string'){
				body = JSON.parse(body);
			}
			if (!error && response.statusCode == 200 && body.success) {
				var formData = {secret : config.secretKey, client_resource_id : body.uuid};
				transact.uuid = body.uuid;

				var transactId = transact._id;

				gated.save(function(err, finalGated){
					if (!isEmpty(gated)){
						var transaction = finalGated.trustTransactions.id(transactId);

						formData.trustline = {
							"limit": "10000000000000",
							"currency": transaction.currency,
							"counterparty": finalGated.rippleAddress,
							"account_allows_rippling": false
						}

						request.post({headers: {'content-type': 'application/json'}, url: config.rippleRest + '/v1/accounts/' + config.publicKey + '/trustlines?validated=true', body: JSON.stringify(formData)}, function(error, response, body) {
							if (typeof body == 'string'){
								body = JSON.parse(body);
							}
							if (!error && response.statusCode == 200 && body.success) {
								transaction.statusUrl = body.status_url;
							}
							else {
								transaction.uuid = '';
							}
							finalGated.save(function(err,gater){

							});
						});
					}
				});
			}
		});
	}

	function checkTrustTransactions(){
		Gateway.find({'trustTransactions.status': 'success'}, function(err, gated){
			if (!isEmpty(gated)){
				gated.forEach(function(gat){
					if (gat.trustTransactions && gat.trustTransactions[0]){
						gat.trustTransactions.forEach(function(transact){
							if (transact.status != 'success' && transact.statusUrl && transact.uuid){
								request(config.rippleRest + transact.statusUrl, function (error, response, body) {
									if (typeof body == 'string'){
										body = JSON.parse(body);
									}
									if (!error && response.statusCode == 200 && body.success) {
										if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
											transact.status = 'success';
											gated.save(function(err, gifted){

											});
										}
										else if (body.payment.state == 'validated'){
											sendTrust(gated, transact);
										}
									}
								});
							}
							else if (!transact.uuid){
								sendTrust(gated, transact);
							}
						});
					}
				});
			}
		});
	}

	function masterInit(){
		getFees();

		paymentsInit('').then(function(lastLedger){
			if (lastLedger && lastLedger[0]){
				client.set('ledgerIndexPier', lastLedger[0]);
			}
		});

		checkTrustTransactions();

		rippleSubscribe();

		escrowChecker();

		setInterval(getFees, 10000);

		function payInit(){
			paymentsInit('').then(function(lastLedger){
				if (lastLedger && lastLedger[0]){
					client.set('ledgerIndexPier', lastLedger[0]);
				}
			});
		}

		setInterval(escrowChecker, 10000);

		setInterval(payInit, 10000);

		setInterval(checkTrustTransactions, 10000);
	}

	masterInit();

	function initWorker(){
		var worked = cluster.fork();
		worked.on('message', function(msg) {
			if (msg.type == 'jobChange'){
				var count = 0;
				var job;
			}
		});
	}

	for (var i = 0; i < numCPUs; i++) {
		initWorker();
	}

	cluster.on('exit', function(worker, code, signal) {
		initWorker();
	});

} else {
	var config = require('./config.js')
		, hbs = require('hbs')
		, http = require('http')
		, mongoose = require('mongoose')
		, fs = require('fs')
		, Chance = require('chance')
		, sha256 = require('fast-sha256')
		, crypto = require('crypto-js')
		, ripple = require('ripple-lib')
		, async = require('async')
		, imC = require('imagemagick-composite')
		, im = require('imagemagick')
		, Q = require('q')
		, gm = require('gm')
		, Snaps = require('snaps').Snaps
		, OAuth= require('oauth').OAuth
		, fbapi = require('facebook-api')
		, flash = require('connect-flash')
		, linkedinClient = require('linkedin-js')(config.inKey, config.inSecret, config.domain)
		, passport = require('passport')
		, LinkedInStrategy = require('passport-linkedin').Strategy
		, FacebookStrategy = require('passport-facebook').Strategy
		, TwitterStrategy = require('passport-twitter').Strategy
		, snapchat = require('snapchat')
		, cookieParser = require('cookie-parser')
		, csv = require('fast-csv')
		, bodyParser  = require('body-parser')
		, qr = require('qrcode')
		, cronJob = require('cron').CronJob
		, nodemailer = require('nodemailer')
		, _ = require('underscore')
		, express = require('express')
		, session = require('express-session')
		, request = require('request')
		, redis = require('redis')
		, session = require('express-session')
		, RedisStore = require('connect-redis')(session)
		, autoIncrement = require('mongoose-auto-increment');

	var Remote = ripple.Remote
	, remote = new Remote({
		servers: config.servers
	});

	function getTwitterFriends(token, tokenSecret, id){
		var defer = Q.defer();
		var client = new Twitter({
			consumer_key: config.twitterKey,
			consumer_secret: config.twitterSecret,
			access_token_key: token,
			access_token_secret: tokenSecret
		});
		client.getFriendsIds(id, function(error, response){
			if (!error && response && response[0]){
				var arrayArray = [];
				var finalArray = [];

				var i,j,temparray,chunk = 100;
				for (i=0,j=ids.length; i<j; i+=chunk) {
					temparray = response.slice(i,i+chunk);
					arrayArray.push(temparray);
				}

				async.map(arrayArray, function (item, complete) {
					client.lookupUsers(item, function(error, response){
						if (!error && response && response[0]){
							response.forEach(function(pusher){
								pusher.text = pusher.screen_name;
								pusher.realid = pusher.id;
								finalArray.push(pusher);
							});
							complete(null, item);
						}
						else {
							complete(null, null);
						}
					});

				}, function (err, results) {
					defer.resolve(finalArray);
				});
			}
		});

		return defer.promise;
	}

	function getTwitterFollowers(token, tokenSecret, id){
		var defer = Q.defer();
		var client = new Twitter({
			consumer_key: config.twitterKey,
			consumer_secret: config.twitterSecret,
			access_token_key: token,
			access_token_secret: tokenSecret
		});
		client.getFollowersIds(id, function(error, response){
			if (!error && response && response[0]){
				var arrayArray = [];
				var finalArray = [];

				var i,j,temparray,chunk = 100;
				for (i=0,j=ids.length; i<j; i+=chunk) {
					temparray = response.slice(i,i+chunk);
					arrayArray.push(temparray);
				}

				async.map(arrayArray, function (item, complete) {
					client.lookupUsers(item, function(error, response){
						if (!error && response && response[0]){
							response.forEach(function(pusher){
								pusher.text = pusher.screen_name;
								pusher.realid = pusher.id;
								finalArray.push(pusher);
							});
							complete(null, item);
						}
						else {
							complete(null, null);
						}
					});

				}, function (err, results) {
					defer.resolve(finalArray);
				});
			}
		});
		return defer.promise;
	}

	function updateFacebookFriends (accessToken){
		var defer = Q.defer();
		var fbclient = fbapi.user(accessToken);
		fbclient.me.friends(
			function(err, data){
				data = _.map(data, function(dat){
					dat.text = dat.name;
					dat.id = Number(dat.id);
					dat.realid = Number(dat.id);
					return dat;
				});
				defer.resolve(data);
			});
		return defer.promise;
	}

	function getLinkedinConnections (token, tokenSecret){
		var defer = Q.defer();
		linkedinClient.apiCall('GET', '/people/~/connections:(id,headline,first-name,last-name,positions:(title),picture-url,public-profile-url)',
			{
				token: {
					oauth_token_secret: tokenSecret
					, oauth_token: token
				}
			}, function (error, result) {
				if (result){
					var count = 0;
					result = _.map(result, function(dat){
						count += 1;
						dat.text = dat.firstName + ' ' + dat.lastName;
						dat.id = Number(dat.id);
						dat.realid = count;
						return dat;
					});
					defer.resolve(result);
				}
			}
		);
		return defer.promise;
	}

	function hex2a(hex) {
		var str = '';
		for (var i = 0; i < hex.length; i += 2)
			str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		return str;
	}
	function encryptToDb (input){
		return crypto.AES.encrypt(input, config.cryptoPhrase).toString();
	}
	function decryptFromDb (input){
		return hex2a(crypto.AES.decrypt(input, config.cryptoPhrase).toString());
	}

	function createQr(id, queryString){
		id = id.toString();
		var deferred = Q.defer();
		qr.save('/home/ubuntu/providence/' + id + '.png', queryString, function(errr,url){
			imC.composite(['-gravity','center','/home/ubuntu/providence/www/img/providenceqr.png', '/home/ubuntu/providence/' + id + '.png', '/home/ubuntu/providence/' + id + '.png'],
				function(errorr, stdout, stderr){
					im.convert(['/home/ubuntu/providence/' + id + '.png', '+level-colors', '#14274d,#ffffff', '/home/ubuntu/providence/' + id + '.png'],
						function(err, stdout){
							fs.readFile('/home/ubuntu/providence/' + id + '.png', function(errrr, original_data){
								var base64 = new Buffer(original_data, 'binary').toString('base64');
								fs.unlinkSync('/home/ubuntu/providence/' + id + '.png');
								deferred.resolve('data:image/png;base64,' + base64);
							});
						});
				});
		});
		return deferred.promise;
	}

	mongoose.connect(config.mongodb);

	var Schema = mongoose.Schema;
	autoIncrement.initialize(mongoose);

	require('./models.js').make(Schema, mongoose, autoIncrement);

	var express = require('express');
	var app = express();

	app.engine('hbs', hbs.__express);
	app.set('view engine', 'hbs');
	app.set('views', __dirname + '/views');

	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	app.use(function(req, res, next) {
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
		res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
		res.header("Access-Control-Allow-Origin", "*");
		res.header('Access-Control-Allow-Methods', 'GET, POST');
		res.header("Access-Control-Allow-Headers", "X-Requested-With");
		res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
		next();
	});

	app.use(cookieParser('Gtahd87836ewfew84rf4fdd73bde9'));

	app.use(session({ secret: 'Gtahd87836ewfew84rf4fdd73bde9', cookie: { domain : "pier.exchange" }, key: 'express.sid', store: new RedisStore }));

	app.use(flash());

	app.use(passport.initialize());
	app.use(passport.session());

	app.use(function(err, req, res, next) {
		res.status(err);
		res.send(http.STATUS_CODES[err]);
	});

	passport.use(new FacebookStrategy({
			clientID: config.fbClientKey,
			clientSecret: config.fbClientSecret,
			callbackURL: "https://api.pier.exchange/auth/facebook/callback",
			passReqToCallback: true
		},
		function(req, accessToken, refreshToken, profile, done) {
			req.session.facebookAccessToken = accessToken;
			req.session.facebookRefreshToken = refreshToken;
			req.session.facebookId = profile.id;
			req.session.save(function(){

			});
			return done(null, profile.id, {facebookId : profile.id, facebookAccessToken : accessToken, facebookRefreshToken : refreshToken});
		}
	));

	passport.use(new LinkedInStrategy({
			consumerKey: config.inKey,
			consumerSecret: config.inSecret,
			callbackURL: "https://api.pier.exchange/auth/linkedin/callback",
			passReqToCallback: true
		},
		function(req, token, tokenSecret, profile, done) {
			req.session.linkedinAccessToken = token;
			req.session.linkedinRefreshToken = tokenSecret;
			req.session.linkedinId = profile.id;
			req.session.save(function(){

			});
			return done(null, profile.id, {linkedinId : profile.id, linkedinAccessToken : token, linkedinRefreshToken : tokenSecret});
		}
	));

	passport.use(new TwitterStrategy({
			consumerKey: config.twitterKey,
			consumerSecret: config.twitterSecret,
			callbackURL: "https://api.pier.exchange/auth/twitter/callback",
			passReqToCallback: true
		},
		function(req, token, tokenSecret, profile, done) {
			req.session.twitterAccessToken = token;
			req.session.twitterinRefreshToken = tokenSecret;
			req.session.twitterId = profile.id;
			console.log(req.sessionID);
			req.session.save(function(){

			});
			return done(null, profile.id, {twitterId : profile.id, twitterAccessToken : token, twitterinRefreshToken : tokenSecret});
		}
	));

	passport.serializeUser(function(user, done) {
		done(null, user);
	});

	passport.deserializeUser(function(id, done) {
		done(null, id);
	});

	function noNetNeutrality(req, res, next) {
		if (!isEmpty(req.body) && ((typeof req.body.rippleAddress == 'string' && ripple.UInt160.is_valid(req.body.rippleAddress)) || typeof req.body.rippleName == 'string') && typeof req.body.hash == 'string' && typeof req.body.signedDateTime == 'string'){
			var rippleFunc = function(body){
				remote.requestTransaction(req.body.hash, function(err,transaction){
					if (transaction && body.address == transaction.Account && transaction.Memos && transaction.Memos[0] && transaction.Amount && ((typeof transaction.Amount === 'object' && transaction.Amount.currency == 'PRV' && transaction.Amount.issuer == config.publicKey && Number(transaction.Amount.value) >= .01) || (typeof transaction.Amount === 'string' && (Number(transaction.Amount) / 1000000) >= .01))){
						var signature;
						transaction.Memos.forEach(function(memo){
							memo = memo.Memo;
							if (memo.MemoType && ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'request'){
								signature = true;
							}
						});
						if (signature){
							var today = new Date();
							var todayPlusMinute = new Date(today.getTime() + 60000);
							var todayMinusMinute = new Date(today.getTime() - 60000);
							var transactionDate = new Date((Number(transaction.date) + 946684800) * 1000);
							if (transactionDate <= todayPlusMinute && transactionDate >= todayMinusMinute){
								ripple.Message.verifyMessageSignature({message : transaction.date.toString(), account : body.address, signature : req.body.signedDateTime}, remote, function(err, isValid){
									if (isValid){
										next();
									}
									else {
										return next(402);
									}
								});
							}
							else {
								return next(402);
							}
						}
						else {
							return next(402);
						}
					}
					else {
						return next(402);
					}
				});
			}
			if (req.body.rippleAddress){
				var body = {address : req.body.rippleAddress};
				if (remote.isConnected()){
					rippleFunc(body);
				}
				else {
					remote.connect(function() {
						rippleFunc(body);
					});
				}
			}
			else {
				request(config.blobVault + req.body.rippleName, function (error, response, body) {
					if (body){
						body = JSON.parse(body);
						if (remote.isConnected()){
							rippleFunc(body);
						}
						else {
							remote.connect(function() {
								rippleFunc(body);
							});
						}
					}
					else {
						return next(402);
					}
				});
			}
		}
		else {
			return next(402);
		}
	}

	Array.prototype.getUnique = function(){
		var u = {}, a = [];
		for(var i = 0, l = this.length; i < l; ++i){
			if(u.hasOwnProperty(this[i])) {
				continue;
			}
			a.push(this[i]);
			u[this[i]] = 1;
		}
		return a;
	}

	function isValidUrl(url) {
		return url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
	}

	function isValid(str) { return /^[A-Za-z0-9]+$/.test(str); }

	function initiateTrust(gated, currency){
		request(config.rippleRest + '/v1/uuid', function (error, response, body) {
			if (typeof body == 'string'){
				body = JSON.parse(body);
			}
			var transac = new TrustTransaction({date : new Date(), uuid : body.uuid, currency : currency});
			gated.trustTransactions.push(transac);

			var transactId = transac._id;

			gated.save(function(err, gifted){
				if (!isEmpty(gifted)){
					var transaction = gifted.trustTransactions.id(transactId);
					if (!error && response.statusCode == 200 && body.success) {
						var formData = {secret : config.secretKey, client_resource_id : body.uuid};
						formData.trustline = {
							"limit": "10000000000000",
							"currency": currency,
							"counterparty": gated.rippleAddress,
							"account_allows_rippling": false
						}

						request.post({headers: {'content-type': 'application/json'}, url: config.rippleRest + '/v1/accounts/' + config.publicKey + '/trustlines?validated=true', body: JSON.stringify(formData)}, function(error, response, body) {
							if (typeof body == 'string'){
								body = JSON.parse(body);
							}
							if (!error && response.statusCode == 200 && body.success) {
								transaction.statusUrl = body.status_url;
							}
							else {
								transaction.uuid = '';
							}
							gated.save(function(err,gater){

							});
						});
					}
					else {
						gated.save(function(err,gater){

						});
					}
				}
			});
		});
	}

	app.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email', 'read_friendlists', 'user_about_me', 'user_activities', 'user_birthday', 'user_education_history', 'publish_actions', 'friends_about_me', 'friends_activities', 'user_checkins', 'friends_birthday', 'friends_education_history', 'friends_checkins', 'user_events', 'friends_events', 'user_groups', 'friends_groups', 'user_hometown', 'friends_hometown', 'user_interests', 'friends_interests', 'user_likes', 'friends_likes', 'user_location', 'friends_location', 'user_status', 'friends_status', 'user_subscriptions', 'friends_subscriptions', 'user_videos', 'user_work_history', 'friends_work_history', 'user_work_history', 'friends_work_history']}));
	app.get('/auth/facebook/callback', function(req, res, next) {
		passport.authenticate('facebook', {scope: ['email', 'read_friendlists', 'user_about_me', 'user_activities', 'user_birthday', 'user_education_history', 'publish_actions', 'friends_about_me', 'friends_activities', 'user_checkins', 'friends_birthday', 'friends_education_history', 'friends_checkins', 'user_events', 'friends_events', 'user_groups', 'friends_groups', 'user_hometown', 'friends_hometown', 'user_interests', 'friends_interests', 'user_likes', 'friends_likes', 'user_location', 'friends_location', 'user_status', 'friends_status', 'user_subscriptions', 'friends_subscriptions', 'user_videos', 'user_work_history', 'friends_work_history', 'user_work_history', 'friends_work_history']},
			function(err, user, info) {
				req.session.facebookAccessToken = info.facebookAccessToken;
				req.session.facebookRefreshToken = info.facebookRefreshToken;
				req.session.facebookId = info.facebookId;
				res.render('auth_close', {
					title: "Facebook Auth",
					body: "<div class='alert alert-success' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important;font-family: \'neon_80sregular\';'>Facebook authentication successful.</p></div>"
				});
			})(req, res, next);
	});

	app.get('/auth/linkedin',passport.authenticate('linkedin', { scope: ['r_fullprofile', 'r_emailaddress','r_contactinfo','r_network','rw_groups','rw_nus','w_messages'] }));
	app.get('/auth/linkedin/callback', function(req, res, next) {
		passport.authenticate('linkedin', { scope: ['r_fullprofile', 'r_emailaddress','r_contactinfo','r_network','rw_groups','rw_nus','w_messages'] }, function(err, user, info) {
			req.session.linkedinAccessToken = info.linkedinAccessToken;
			req.session.linkedinRefreshToken = info.linkedinRefreshToken;
			req.session.linkedinId = info.linkedinId;
			req.session.save(function(){

			});
			res.render('auth_close', {
				title: "LinkedIn Auth",
				body: "<div class='alert alert-success' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important;font-family: \'neon_80sregular\';'>LinkedIn authentication successful.</p></div>"
			});
		})(req, res, next);
	});

	app.get('/auth/twitter', passport.authenticate('twitter'));
	app.get('/auth/twitter/callback', function(req, res, next) {
		passport.authenticate('twitter', function(err, user, info) {
			req.session.twitterAccessToken = info.twitterAccessToken;
			req.session.twitterinRefreshToken = info.twitterinRefreshToken;
			req.session.twitterId = info.twitterId;
			req.session.save(function(){

			});
			res.render('auth_close', {
				title: "Twitter Auth",
				body: "<div class='alert alert-success' style='height: 100% !important; display: table !important;  width: 100% !important;text-align:center !important; vertical-align:middle !important; font-size: 18px !important;'><p style='display: table-cell !important;  text-align: center !important;  vertical-align: middle !important;font-family: \'neon_80sregular\';'>Twitter authentication successful.</p></div>"
			});
		})(req, res, next);
	});

	app.post('/isApproved', function(req, res){
		if (typeof req.body.rippleName == 'string' && req.body.rippleName.length < 100 && typeof req.body.currency == 'string' && req.body.currency.length == 3){
			Gateway.findOne({$or: [{rippleName: req.body.rippleName}, {rippleName: '~' + req.body.rippleName}]}, '-email', function(err, gate){
				if (!isEmpty(gate) && gate.currencies && gate.currencies[0]){
					var inCurrency;
					gate.currencies.forEach(function(curr){
						if (curr == req.body.currency){
							inCurrency = true;
						}
					});
					if (inCurrency){
						res.send({success : gate});
					}
					else {
						res.send({error : 'Error'});
					}
				}
				else {
					res.send({error : 'Error'});
				}
			});
		}
	});

	app.post('/unlink', function(req, res){
		req.session.facebookAccessToken = '';
		req.session.facebookRefreshToken = '';
		req.session.linkedinAccessToken = '';
		req.session.linkedinRefreshToken = '';
		req.session.twitterAccessToken = '';
		req.session.twitterinRefreshToken = '';
		res.send({success : 'success'});
	});

	app.post('/linked', function(req, res){
		var facebook;
		var linkedin;
		var twitter;
		if (req.session.facebookAccessToken){
			facebook = true;
		}
		if (req.session.linkedinAccessToken){
			linkedin = true;
		}
		if (req.session.twitterAccessToken){
			twitter = true;
		}
		console.log(req.sessionID);
		req.session.store = true;
		req.session.save(function(){

		});
		res.send({facebook : facebook, linkedin : linkedin, twitter : twitter});
	});

	function releaseEscrow(escrow, repeat, address){
		var defer = Q.defer();
		if (!repeat.returned){
			request(config.rippleRest + '/v1/transactions/' + decryptFromDb(repeat.transactionHash), function (error, response, body) {
				if (!error && response.statusCode == 200 && body.success) {
					request(config.rippleRest + '/v1/uuid', function (error, response, body) {
						if (!error && response.statusCode == 200 && body.success) {
							repeat.uuid = body.uuid;
							repeat.addressTo = address;
							escrow.save(function(err,finCash){
								if (isEmpty(err)){
									var rippleFunc = function(){
										var request = Remote.requestAccountInfo({account : 'raao4egwGfCNWxCUg1jqzzZL5BY9Rzrre9'}, function(err, info) {
											if (err && err.remote && err.remote.err == 'actNotFound'){
												repeat.uuid = '';
												repeat.fundingUuid = body.uuid;
												if ((((!isNaN(Number(body.transaction.Amount)) && Number(body.transaction.Amount) <= repeat.value) ||  (repeat.value <= body.transaction.Amount.value))) && (repeat.currency == body.transaction.Amount.currency) && ((!repeat.issuer && !body.transaction.Amount.issuer) || (repeat.issuer == body.transaction.Amount.issuer))){
													var request = Remote.requestServerInfo();
													request.request(function(err, res) {
														if (res && res.info && res.info.validated_ledger && res.info.validated_ledger.reserve_base_xrp && res.info.validated_ledger.reserve_inc_xrp){
															var reserve = Number(res.info.validated_ledger.reserve_base_xrp) + Number(res.info.validated_ledger.reserve_inc_xrp) + 1;
															request(config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments/paths/' + address + '/' + reserve.toString() + '+' + 'XRP' + '?source_currencies=' + repeat.currency + '+' + repeat.issuer, function (error, response, body) {
																if (typeof body == 'string'){
																	body = JSON.parse(body);
																}
																if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {

																	var finalPayment;

																	body.payments.forEach(function(payment){
																		if (payment.source_amount && payment.source_amount.currency == repeat.currency && repeat.value >= Number(payment.source_amount.value)){
																			if (finalPayment && Number(finalPayment.source_amount.value) > Number(payment.source_amount.value)){
																				finalPayment = payment;
																			}
																			else {
																				finalPayment = payment;
																			}
																		}
																	});

																	if (finalPayment){
																		var formData = {secret : config.secretKey, client_resource_id : finCash.fundingUuid, payment : finalPayment};

																		formData.payment.memos = [{MemoType : 'Escrow Release', MemoData : JSON.stringify({hashReference : escrow.transactionHash})}];
																		formData.payment.source_tag = finCash.destination_tag;

																		request.post({headers: {'content-type' : 'application/json'}, body: JSON.stringify(formData), url: config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments?validated=true'}, function optionalCallback(err, httpResponse, body) {
																			if (typeof body == 'string'){
																				body = JSON.parse(body);
																			}
																			if (!error && response.statusCode == 200 && body.success) {
																				if (!repeat.beforeValue){
																					repeat.beforeValue = repeat.value;
																				}
																				repeat.value = repeat.beforeValue - Number(payment.source_amount.value);
																				repeat.fundingStatusUrl = body.status_url;
																				finCash.save(function(err, doned){

																				});
																			}
																			else {
																				repeat.fundingUuid = '';
																				finCash.save(function(err, doned){

																				});
																			}
																		});
																	}
																}
																else {
																	repeat.fundingUuid = '';
																	finCash.save(function(err, doned){

																	});
																}
															});
														}
													});
												}
											}
											else if (!err && info){
												if ((((!isNaN(Number(body.transaction.Amount)) && Number(body.transaction.Amount) <= repeat.value) ||  (repeat.value <= body.transaction.Amount.value))) && (repeat.currency == body.transaction.Amount.currency) && ((!repeat.issuer && !body.transaction.Amount.issuer) || (repeat.issuer == body.transaction.Amount.issuer))){
													request(config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments/paths/' + address + '/' + (Number(repeat.value) * 0.005).toFixed(6) + '+' + repeat.currency + '+' + repeat.issuer +  '?source_currencies=' + repeat.currency + '+' + repeat.issuer, function (error, response, body) {
														if (typeof body == 'string'){
															body = JSON.parse(body);
														}
														if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {
															var formData = {secret : config.secretKey, client_resource_id : finCash.uuid, payment : body.payments[0]};

															formData.payment.memos = [{MemoType : 'Escrow Release', MemoData : JSON.stringify({hashReference : escrow.transactionHash})}];
															formData.payment.source_tag = finCash.destination_tag;

															request.post({headers: {'content-type' : 'application/json'}, body: JSON.stringify(formData), url: config.rippleRest + '/v1/accounts/' + config.publicKey + '/payments?validated=true'}, function optionalCallback(err, httpResponse, body) {
																if (typeof body == 'string'){
																	body = JSON.parse(body);
																}
																if (!error && response.statusCode == 200 && body.success) {
																	repeat.statusUrl = body.status_url;
																	finCash.save(function(err, doned){

																	});
																}
																else {
																	repeat.uuid = '';
																	finCash.save(function(err, doned){

																	});
																}
															});
														}
														else {
															repeat.uuid = '';
															finCash.save(function(err, doned){

															});
														}
													});
												}
											}
										});

									}
									if (Remote.isConnected()){
										rippleFunc();
									}
									else {
										Remote.connect(function() {
											rippleFunc();
										});
									}
								}
							});
						}
					});
				}
			});
		}
		return defer.promise;
	}

	app.get('/amountLookup', function(req, res){
		if (req.query && req.query.code && typeof req.query.code == 'string'){
			req.query.code = req.query.code.trim();
			Escrow.findOne({$or : [{'repeats.redeemCode' : encryptToDb(req.query.code)}, {'repeats.neumonicWords' : encryptToDb(req.query.code)}]},function(err,escrowed){
				if (!isEmpty(escrowed)){
					var repeat;
					escrowed.repeats.forEach(function(rep){
						if ((escrowed.type != 'snapchat' && decryptFromDb(rep.redeemCode) == req.query.code) || (escrowed.type == 'snapchat' && decryptFromDb(rep.neumonicWords) == req.query.code)){
							repeat = rep;
						}
					});
					res.send({transactionHash : repeat.transactionHash});
				}
			});
		}
	});

	app.post('/redeem', function(req, res){
		if (req.body && req.body.code && typeof req.body.code == 'string' && req.body.code.length <= 1000 && typeof req.body.rippleAddress == 'string' && ripple.UInt160.is_valid(req.body.rippleAddress)){
			req.body.code = req.body.code.trim();
			Escrow.findOne({$or : [{'repeats.redeemCode' : encryptToDb(req.body.code)}, {'repeats.neumonicWords' : encryptToDb(req.body.code)}]},function(err,escrowed){
				if (!isEmpty(escrowed)){
					var repeat;
					escrowed.repeats.forEach(function(rep){
						if ((escrowed.type != 'snapchat' && decryptFromDb(rep.redeemCode) == req.body.code) || (escrowed.type == 'snapchat' && decryptFromDb(rep.neumonicWords) == req.body.code)){
							repeat = rep;
						}
					});
					if (repeat && !repeat.uuid && repeat.status != 'success'){
						releaseEscrow(escrowed, repeat, req.body.rippleAddress).then(function(){
							res.send({success: 'Escrow released.'});
						});
					}
				}
				else {
					res.send({error: 'Code not found.'});
				}
			});
		}
	});

	app.post('/sms', noNetNeutrality, function(req, res){
		if (typeof req.body.from == 'string' && req.body.from.length <= 1000 && typeof req.body.message == 'string' && req.body.message.length <= 1000 && typeof req.body.recipients == 'string' && req.body.recipients.length < 2000 && typeof req.body.currency == 'string' && req.body.currency.length == 3 && typeof req.body.issuer == 'string' && req.body.issuer.length < 30 && ripple.UInt160.is_valid(req.body.issuer)){
			var transaction = new Escrow({date : new Date(), type : 'sms', recipients : req.body.recipients.toString(), from : req.body.from, message : req.body.message, rippleName : req.body.rippleName, rippleAddress : req.body.rippleAddress});
			transaction.save(function(err, transact){
				if (!isEmpty(transact)){
					var destinationTag = transact.destinationTag;
					createQr(transact._id.toString(), 'https://rippletrade.com/#/send?to=~OurProvidence&currency=' + req.body.currency + '&issuer=' + req.body.issuer + '&dt=' + destinationTag.toString()).then(function(qr){
						res.send({qr : qr, dt : destinationTag});
					});
				}
			});
		}
	});

	app.post('/email', noNetNeutrality, function(req, res){
		if (typeof req.body.email == 'string' && req.body.email.length <= 1000 && (!req.body.name || (typeof req.body.name == 'string' && req.body.name.length <= 1000)) && (!req.body.rippleName || (typeof req.body.rippleName == 'string' && req.body.rippleName.length <= 1000)) && typeof req.body.recipients == 'string' && req.body.recipients.length < 2000 && typeof req.body.currency == 'string' && req.body.currency.length == 3 && typeof req.body.issuer == 'string' && req.body.issuer.length < 30 && ripple.UInt160.is_valid(req.body.issuer)){
			var transaction = new Escrow({date : new Date(), type : 'email', name : req.body.name, rippleName : req.body.rippleName, rippleAddress : req.body.rippleAddress, recipients : req.body.recipients.toString(), email : req.body.email});
			transaction.save(function(err, transact){
				if (!isEmpty(transact)){
					var destinationTag = transact.destinationTag;
					createQr(transact._id.toString(), 'https://rippletrade.com/#/send?to=~OurProvidence&currency=' + req.body.currency + '&issuer=' + req.body.issuer + '&dt=' + destinationTag.toString()).then(function(qr){
						res.send({qr : qr, dt : destinationTag});
					});
				}
			});
		}
	});

	app.post('/twitter', noNetNeutrality, function(req, res){
		if ((!req.body.rippleName || (typeof req.body.rippleName == 'string' && req.body.rippleName.length <= 1000)) && ((req.body.subtype == 'dm' && typeof req.body.subtype == 'string' && req.body.subtype.length <= 1000 && typeof req.body.recipients == 'string' && req.body.recipients.length < 2000) || (req.body.subtype == 'upForGrabs' && typeof req.body.subtype == 'string')) && typeof req.body.currency == 'string' && req.body.currency.length == 3 && typeof req.body.issuer == 'string' && req.body.issuer.length < 30 && ripple.UInt160.is_valid(req.body.issuer)){
			var transaction = new Escrow({date : new Date(), type : 'twitter', subtype : req.body.subtype, socialId : req.session.twitterId, accessToken : encryptToDb(req.session.twitterAccessToken), otherToken : encryptToDb(req.session.twitterinRefreshToken), rippleName : req.body.rippleName, rippleAddress : req.body.rippleAddress, recipients : req.body.recipients.toString()});
			transaction.save(function(err, transact){
				if (!isEmpty(transact)){
					var destinationTag = transact.destinationTag;
					createQr(transact._id.toString(), 'https://rippletrade.com/#/send?to=~OurProvidence&currency=' + req.body.currency + '&issuer=' + req.body.issuer + '&dt=' + destinationTag.toString()).then(function(qr){
						res.send({qr : qr, dt : destinationTag});
					});
				}
			});
		}
	});

	app.post('/linkedin', noNetNeutrality, function(req, res){
		if ((!req.body.rippleName || (typeof req.body.rippleName == 'string' && req.body.rippleName.length <= 1000)) && ((req.body.subtype == 'dm' && typeof req.body.subtype == 'string' && req.body.subtype.length <= 1000 && typeof req.body.recipients == 'string' && req.body.recipients.length < 2000) || (req.body.subtype == 'upForGrabs' && typeof req.body.subtype == 'string')) && typeof req.body.currency == 'string' && req.body.currency.length == 3 && typeof req.body.issuer == 'string' && req.body.issuer.length < 30 && ripple.UInt160.is_valid(req.body.issuer)){
			var transaction = new Escrow({date : new Date(), type : 'linkedin', subtype : req.body.subtype, socialId : req.session.linkedinId, accessToken : encryptToDb(req.session.linkedinAccessToken), otherToken : encryptToDb(req.session.linkedinRefreshToken), rippleName : req.body.rippleName, rippleAddress : req.body.rippleAddress, recipients : req.body.recipients.toString()});
			transaction.save(function(err, transact){
				if (!isEmpty(transact)){
					var destinationTag = transact.destinationTag;
					createQr(transact._id.toString(), 'https://rippletrade.com/#/send?to=~OurProvidence&currency=' + req.body.currency + '&issuer=' + req.body.issuer + '&dt=' + destinationTag.toString()).then(function(qr){
						res.send({qr : qr, dt : destinationTag});
					});
				}
			});
		}
	});

	app.post('/facebook', noNetNeutrality, function(req, res){
		if ((!req.body.rippleName || (typeof req.body.rippleName == 'string' && req.body.rippleName.length <= 1000)) && ((req.body.subtype == 'dm' && typeof req.body.subtype == 'string' && req.body.subtype.length <= 1000 && typeof req.body.recipients == 'string' && req.body.recipients.length < 2000) || (req.body.subtype == 'upForGrabs' && typeof req.body.subtype == 'string')) && typeof req.body.currency == 'string' && req.body.currency.length == 3 && typeof req.body.issuer == 'string' && req.body.issuer.length < 30 && ripple.UInt160.is_valid(req.body.issuer)){
			var transaction = new Escrow({date : new Date(), type : 'facebook', subtype : req.body.subtype, socialId : req.session.facebookId, accessToken : encryptToDb(req.session.facebookAccessToken), otherToken : encryptToDb(req.session.facebookRefreshToken), rippleName : req.body.rippleName, rippleAddress : req.body.rippleAddress, recipients : req.body.recipients.toString()});
			transaction.save(function(err, transact){
				if (!isEmpty(transact)){
					var destinationTag = transact.destinationTag;
					createQr(transact._id.toString(), 'https://rippletrade.com/#/send?to=~OurProvidence&currency=' + req.body.currency + '&issuer=' + req.body.issuer + '&dt=' + destinationTag.toString()).then(function(qr){
						res.send({qr : qr, dt : destinationTag});
					});
				}
			});
		}
	});

	// TODO : make sure SMS in correct format

	app.post('/twitterFriendsFollowers', noNetNeutrality, function(req, res){
		if (typeof req.session.twitterId == 'string' && typeof req.session.twitterAccessToken == 'string' && typeof req.session.twitterinRefreshToken == 'string'){
			getTwitterFriends(req.session.twitterAccessToken, req.session.twitterinRefreshToken, req.session.twitterId).then(function(friends){
				getTwitterFollowers(req.session.twitterAccessToken, req.session.twitterinRefreshToken, req.session.twitterId).then(function(followers){
					res.send({friends : friends, followers : followers});
				});
			});
		}
	});

	app.post('/linkedinConnections', noNetNeutrality, function(req, res){
		if (typeof req.session.linkedinId == 'string' && typeof req.session.linkedinAccessToken == 'string' && typeof req.session.linkedinRefreshToken == 'string'){
			getLinkedinConnections(req.session.linkedinAccessToken, req.session.linkedinRefreshToken).then(function(connections){
				res.send({connections : connections});
			});
		}
	});

	app.post('/facebookConnections', noNetNeutrality, function(req, res){
		if (typeof req.session.facebookId == 'string' && typeof req.session.facebookAccessToken == 'string' && typeof req.session.facebookRefreshToken == 'string'){
			updateFacebookFriends(req.session.facebookAccessToken).then(function(friends){
				res.send({friends : friends});
			});
		}
	});

	app.post('/snapchatConfirm', function(req, res){
		if (typeof req.body.username == 'string' && req.body.username.length <= 1000 && typeof req.body.password == 'string' && req.body.password.length <= 1000){
			(new Snaps(req.body.username, req.body.password)).then(function(snaps) {
				var friends = snaps.getFriends();
				var finalFriends = [];
				if (friends && friends[0]){
					var count = 0;
					friends.forEach(function(ff){
						count += 1;
						finalFriends.push({text : ff.name, id : count, realid : ff.id});
					});
				}
				res.send({success : 'Login correct', friends: finalFriends});
			}).catch(function(err) {
					res.send({success : 'Login incorrect'});
				})
		}
	});

	app.post('/snapchat', noNetNeutrality, function(req, res){
		if (typeof req.body.username == 'string' && typeof req.body.password == 'string' && typeof req.body.recipients == 'string' && req.body.recipients.length < 2000 && typeof req.body.currency == 'string' && req.body.currency.length == 3 && typeof req.body.issuer == 'string' && req.body.issuer.length < 30 && ripple.UInt160.is_valid(req.body.issuer)){
			var transaction = new Escrow({date : new Date(), explainer : req.body.explainer, type : 'snapchat', snapUser: encryptToDb(req.body.username), snapPassword: encryptToDb(req.body.password), rippleName : req.body.rippleName, rippleAddress : req.body.rippleAddress, recipients : req.body.recipients.toString()});
			transaction.save(function(err, transact){
				if (!isEmpty(transact)){
					var destinationTag = transact.destinationTag;
					createQr(transact._id.toString(), 'https://rippletrade.com/#/send?to=~OurProvidence&currency=' + req.body.currency + '&issuer=' + req.body.issuer + '&dt=' + destinationTag.toString()).then(function(qr){
						res.send({qr : qr, dt : destinationTag});
					});
				}
			});
		}
	});

	app.post('/applyForAnchor', noNetNeutrality, function(req, res){
		if (typeof req.body.domained == 'string' && req.body.domained.length < 100 && isValidUrl(req.body.domained) && typeof req.body.currency == 'string' && req.body.currency.length == 3){
			Gateway.findOne({rippleAddress: req.body.rippleAddress}, function(err, gate){
				if (!isEmpty(gate)){
					if (gate.currencies && gate.currencies[0]){
						var isIn;
						gate.currencies.forEach(function(curr){
							if (curr == req.body.currency){
								isIn = true;
							}
						});
						if (!isIn){
							gate.currencies.push(req.body.currency);

							gate.save(function(err, gated){
								res.send({success : req.body.currency + ' applied for successfully.'});
							});
						}
						else {
							res.send({error : 'Already applied for ' + req.body.currency + '.'});
						}
					}
				}
				else if (typeof req.body.email == 'string' && typeof req.body.name == 'string'){
					var gated = new Gateway({date : new Date(), email : req.body.email, type : 'Anchor', name : req.body.name, domained : req.body.domained, currencies : [req.body.currency], rippleAddress : req.body.rippleAddress, rippleName : req.body.rippleName});
					gated.save(function(err,gat){
						res.send({success : req.body.currency + ' applied for successfully.'});
					});
				}
				else {
					res.send({error : 'Fields incorrect.'});
				}
			});
		}
	});

	app.post('/registerDocker', noNetNeutrality, function(req, res){
		if (typeof req.body.domained == 'string' && req.body.domained.length < 100 && isValidUrl(req.body.domained) && typeof req.body.currency == 'string' && req.body.currency.length == 3){
			Gateway.findOne({rippleAddress: req.body.rippleAddress}, function(err, gate){
				if (!isEmpty(gate)){
					if (gate.currencies && gate.currencies[0]){
						var isIn;
						gate.currencies.forEach(function(curr){
							if (curr == req.body.currency){
								isIn = true;
							}
						});
						if (!isIn){
							gate.currencies.push(req.body.currency);

							gate.save(function(err, gated){
								if (!isEmpty(gated)){
									initiateTrust(gated, req.body.currency);
									res.send({success : req.body.currency + ' applied for successfully.'});
								}
							});
						}
						else {
							res.send({error : 'Already applied for ' + req.body.currency + '.'});
						}
					}
				}
				else if (typeof req.body.email == 'string' && typeof req.body.name == 'string'){
					var gated = new Gateway({date : new Date(), email : req.body.email, type : 'Docker', name : req.body.name, domained : req.body.domained, currencies : [req.body.currency], rippleAddress : req.body.rippleAddress, rippleName : req.body.rippleName});
					gated.save(function(err,gat){
						if (!isEmpty(gat)){
							initiateTrust(gat, req.body.currency);
							res.send({success : req.body.currency + ' applied for successfully.'});
						}
					});
				}
				else {
					res.send({error : 'Fields incorrect.'});
				}
			});
		}
	});

	var port;

	if (config.dev){
		port = 4010;
	}
	else {
		port = 3010
	}

	var server = app.listen(port, function () {

	});
}