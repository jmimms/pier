angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ionic.contrib.ui.cards', 'angularMoment', 'ui.gravatar', 'emoticonizeFilter', 'zumba.angular-waypoints', 'LocalForageModule', 'ngRoute', 'ngSanitize', 'linkify', 'ngTagsInput'])

.run(['$ionicPlatform', '$rootScope', '$state', '$http', '$ionicPopup', 'accountServices', '$location', '$anchorScroll', '$sce', '$ionicPopover', '$q', '$localForage', '$sanitize', 'linkify', 'socialFunctions', '$ionicPopover', function($ionicPlatform, $rootScope, $state, $http, $ionicPopup, accountServices, $location, $anchorScroll, $sce, $ionicPopover, $q, $localForage, $sanitize, linkify, socialFunctions, $ionicPopover) {
  $rootScope.rippled = [ 'wss://s1.ripple.com:443', 'wss://rippled.providence.solutions:443' ];
  $rootScope.receiveName = 'OurProvidence';
  $rootScope.receiveAddress = 'rEoazhPt4VJuSs6yB5irTVitjmwsXuqwyN';
  $rootScope.rippleTxtUrl = 'https://pier.exchange/ripple.txt';
  $rootScope.domain = 'pier.exchange';
  var pf;

  var anchors = [{src: 'https://d1pzmogk9nquph.cloudfront.net/img/PierAssets/blue-anchor.svg', cssClass:'anchor-blue'},{src: 'https://d1pzmogk9nquph.cloudfront.net/img/PierAssets/gold-anchor.svg', cssClass:'anchor-gold'},{src: 'https://d1pzmogk9nquph.cloudfront.net/img/PierAssets/green-anchor.svg', cssClass:'anchor-green'},{src: 'https://d1pzmogk9nquph.cloudfront.net/img/PierAssets/navy-anchor.svg', cssClass:'anchor-navy'},{src: 'https://d1pzmogk9nquph.cloudfront.net/img/PierAssets/purple-anchor.svg', cssClass:'anchor-purple'}];

  $rootScope.transmissionMethods = [
	  {
		  name: 'Snapchat'
	  },
	  {
		  name: 'Email'
	  },
	  {
		  name: 'SMS'
	  },
	  {
		  name: 'LinkedIn'
	  },
	  {
		  name: 'Twitter'
	  },
	  {
		  name: 'Facebook'
	  }
  ];

  $rootScope.anchorGateways = [
	{
		rippleName : 'SnapSwap',
		rippleAddress : 'rMwjYedjc7qqtKYVLiAccJSmCwih4LnE2q',
		image : 'snapswap.png',
		domained : 'https://www.snapswap.us',
		name : 'SnapSwap',
		currencies: ['USD', 'EUR']
	},
	{
		rippleName : 'Bitstamp',
		rippleAddress : 'rvYAfWj5gh67oV6fW32ZzP3Aw4Eubs59B',
		image : 'bitstamp.png',
		domained : 'https://www.bitstamp.net',
		name : 'Bitstamp',
		currencies : ['BTC', 'USD']
	},
	{
		rippleName : 'RippleCN',
		rippleAddress : 'rnuF96W4SZoCJmbHYBFoJZpR8eCaxNvekK',
		image : 'ripplecn.png',
		domained : 'https://www.ripplecn.com',
		name : 'RippleCN',
		currencies : ['BTC', 'CNY', 'LTC']
	},
	{
		rippleName : 'TheRock',
		rippleAddress : 'rLEsXccBGNR3UPuPu2hUXPjziKC3qKSBun',
		image : 'therock.png',
		domained : 'https://www.therocktrading.com',
		name : 'TheRock',
		currencies : ['BTC', 'LTC', 'SLL', 'EUR', 'USD']
	},
	{
		rippleName : 'SnapSwap',
		rippleAddress : 'rMwjYedjc7qqtKYVLiAccJSmCwih4LnE2q',
		image : 'snapswap.png',
		domained : 'https://www.btc2ripple.com',
		name : 'BTC2Ripple',
		currencies : ['BTC']
	},
	{
		rippleName : 'RippleChina',
		rippleAddress : 'razqQKzJRdB4UxFPWf5NEpEG3WMkmwgcXA',
		image : 'ripplechina.png',
		domained : 'https://trade.ripplechina.net',
		name : 'RippleChina',
		currencies : ['BTC', 'CNY', 'LTC']
	},
	{
		rippleName : 'Coinex',
		rippleAddress : 'rsP3mgGb2tcYUrxiLFiHJiQXhsziegtwBc',
		image : 'coinex.png',
		domained : 'https://coinexgateway.com',
		name : 'Coinex',
		currencies : ['NZD', 'BTC', 'USD', 'AUD']
	},
	{
		rippleName : 'Bitso',
		rippleAddress : 'rG6FZ31hDHN1K5Dkbma3PSB5uVCuVVRzfn',
		image : 'bitso.png',
		domained : 'https://bitso.com',
		name : 'Bitso',
		currencies : ['MXN', 'USD', 'BTC']
	},
	{
		rippleName : 'Justcoin-Hot',
		rippleAddress : 'rJHygWcTLVpSXkowott6kzgZU6viQSVYM1',
		image : 'justcoin.png',
		domained : 'https://justcoin.com/',
		name : 'Justcoin',
		currencies : ['NOK', 'BTC', 'LTC']
	}
  ];

  $rootScope.info = {};
  $rootScope.info.backup = true;

  $rootScope.assets = {};

  $rootScope.assets.anchor = anchors[Math.floor(Math.random()*anchors.length)];

  $rootScope.signatures = {};
  $rootScope.signatures.signatures = [];

  $rootScope.chatData = {};
  $rootScope.chatData.messages = [];

  $rootScope.loggedInAccount = {};
  $rootScope.balances = {};

  $rootScope.packet = {};

  $rootScope.pledge = {};

  $rootScope.gatewayInfo = {};

  $rootScope.data = {};

  $rootScope.data.gatewaySelected = {};

  $rootScope.data.section = 'ship';

  $rootScope.data.shippingSection = 'transmissionSelect';

  $rootScope.twitterPacket = {};

  $rootScope.twitterPacket.dm = true;

  $rootScope.facebookPacket = {};

  $rootScope.facebookPacket.dm = true;

  $rootScope.linkedinPacket = {};

  $rootScope.linkedinPacket.dm = true;

  $rootScope.data.date = new Date();

		// TODO : redeem panel, trust listen on unfunded, redeem on url signin

	var snapSearch = lunr(function () {
		this.field('text', { boost: 10 })
	});

	var facebookSearch = lunr(function () {
		this.field('text', { boost: 10 })
	});

	var linkedinSearch = lunr(function () {
		this.field('text', { boost: 10 })
	});

	var twitterSearch = lunr(function () {
		this.field('text', { boost: 10 }),
		this.field('name')
	});

	var twitterFriendsSearch = lunr(function () {
		this.field('text', { boost: 10 })
	});

	$rootScope.snapChatAuto = function(query){
		var index = snapSearch.search(query);
		var searchResults = [];
		if (index && index[0]){
			index.forEach(function(result){
				var resed = _.find($rootScope.snapChatPacket.friends, function(chr) {
					return chr.id == result.id;
				});
				if (resed){
					searchResults.push(resed);
				}
			});
		}
		return searchResults;
	}

	$rootScope.facebookAuto = function(query){
		var index = facebookSearch.search(query);
		var searchResults = [];
		if (index && index[0]){
			index.forEach(function(result){
				var resed = _.find($rootScope.facebookPacket.friends, function(chr) {
					return chr.id == result.id;
				});
				if (resed){
					searchResults.push(resed);
				}
			});
		}
		return searchResults;
	}

	$rootScope.linkedinAuto = function(query){
		var index = linkedinSearch.search(query);
		var searchResults = [];
		if (index && index[0]){
			index.forEach(function(result){
				var resed = _.find($rootScope.linkedinPacket.connections, function(chr) {
					return chr.id == result.id;
				});
				if (resed){
					searchResults.push(resed);
				}
			});
		}
		return searchResults;
	}

	$rootScope.twitterFriendsAuto = function(query){
		var index = twitterFriendsSearch.search(query);
		var searchResults = [];
		if (index && index[0]){
			index.forEach(function(result){
				var resed = _.find($rootScope.twitterPacket.friends, function(chr) {
					return chr.id == result.id;
				});
				if (resed){
					searchResults.push(resed);
				}
			});
		}
		return searchResults;
	}

	$rootScope.twitterFollowersAuto = function(query){
		var index = twitterSearch.search(query);
		var searchResults = [];
		if (index && index[0]){
			index.forEach(function(result){
				var resed = _.find($rootScope.twitterPacket.followers, function(chr) {
					return chr.id == result.id;
				});
				if (resed){
					searchResults.push(resed);
				}
			});
		}
		return searchResults;
	}
  $rootScope.disconnectSocial = function(){
      var defer = $q.defer();
	  $http.post('https://api.pier.exchange/unlink').
		  success(function(data, status, headers, config) {
		      defer.resolve();
		  }).
		  error(function(data, status, headers, config) {

		  });
	  return defer.promise;
  }
  $rootScope.checkSocialConnection = function(){
	  $http.post('https://api.pier.exchange/linked').
		  success(function(data, status, headers, config) {
			  $rootScope.data.facebookConnected = data.facebook;
			  $rootScope.data.twitterConnected = data.twitter;
			  $rootScope.data.linkedinConnected = data.linkedin;
			  if (data.facebook){
				  $http.post('https://api.pier.exchange/facebookConnections').
					  success(function(data, status, headers, config) {
						  console.log(data);
						  $rootScope.facebookPacket.friends = data.friends;
						  if ($rootScope.facebookPacket.friends && $rootScope.facebookPacket.friends[0]){
							  $rootScope.facebookPacket.friends.forEach(function(friend){
								  facebookSearch.add(friend);
							  });
						  }
					  }).
					  error(function(data, status, headers, config) {

					  });
			  }
			  if (data.twitter){
				  $http.post('https://api.pier.exchange/twitterFriendsFollowers').
					  success(function(data, status, headers, config) {
						  console.log(data);
						  $rootScope.twitterPacket.friends = data.friends;
						  $rootScope.twitterPacket.followers = data.followers;
						  if ($rootScope.twitterPacket.friends && $rootScope.twitterPacket.friends[0]){
							  $rootScope.twitterPacket.friends.forEach(function(friend){
								  twitterFriendsSearch.add(friend);
							  });
						  }
						  if ($rootScope.twitterPacket.followers && $rootScope.twitterPacket.followers[0]){
							  $rootScope.twitterPacket.followers.forEach(function(friend){
								  twitterSearch.add(friend);
							  });
						  }
					  }).
					  error(function(data, status, headers, config) {

					  });
			  }
			  if (data.linkedin){
				  $http.post('https://api.pier.exchange/linkedinConnections').
					  success(function(data, status, headers, config) {
						  console.log(data);
						  $rootScope.linkedinPacket.connections = data.connections;
						  if ($rootScope.linkedinPacket.connections && $rootScope.linkedinPacket.connections[0]){
							  $rootScope.linkedinPacket.connections.forEach(function(friend){
								  linkedinSearch.add(friend);
							  });
						  }
					  }).
					  error(function(data, status, headers, config) {

					  });
			  }
		  }).
		  error(function(data, status, headers, config) {

		  });
  }

  $rootScope.connectTwitter = function(){
	  var left = (screen.width/2)-(400/2);
	  var top = (screen.height/2)-(700/2);
	  myWindow = window.open('https://api.pier.exchange/auth/twitter', 'Twitter Login', 'width=400, height=700, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

	  var pollTimer = window.setInterval(function() {
		  if (myWindow.closed !== false) {
			  $rootScope.checkSocialConnection();
			  window.clearInterval(pollTimer);
		  }
	  }, 200);
  }

  $rootScope.connectFacebook = function(){
	  var left = (screen.width/2)-(400/2);
	  var top = (screen.height/2)-(600/2);
	  myWindow = window.open('https://api.pier.exchange/auth/facebook', 'Facebook Login', 'width=1100, height=600, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

	  var pollTimer = window.setInterval(function() {
		  if (myWindow.closed !== false) {
			  $rootScope.checkSocialConnection();
			  window.clearInterval(pollTimer);
		  }
	  }, 200);
  }

  $rootScope.connectLinkedin = function(){
	  var left = (screen.width/2)-(400/2);
	  var top = (screen.height/2)-(700/2);
	  myWindow = window.open('https://api.pier.exchange/auth/linkedin', 'LinkedIn Login', 'width=400, height=700, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, top='+top+', left='+left);

	  var pollTimer = window.setInterval(function() {
		  if (myWindow.closed !== false) {
			  $rootScope.checkSocialConnection();
			  window.clearInterval(pollTimer);
		  }
	  }, 200);
  }

  $rootScope.switchSection = function(swit){
	  $rootScope.data.section = swit;
  }

  var Remote = ripple.Remote;
  $rootScope.remote = new Remote({
	servers: $rootScope.rippled
  });

  $rootScope.globalRemote = new Remote({
	servers: $rootScope.rippled
  });

  $ionicPopover.fromTemplateUrl('templates/anchor-popover.html', {
	scope: $rootScope
  }).then(function(popover) {
		$rootScope.anchorPopover = popover;
	});

  $rootScope.openAnchorPopover = function($event) {
	  $rootScope.popoverOpen = 'Anchor';
	  $rootScope.data.shippingSection = 'transmissionSelect';
	  $rootScope.data.outType = '';
	  $rootScope.data.currency = '';
	  $rootScope.data.gatewaySelected = {};
	  $rootScope.anchorPopover.show($event);
  }

  $rootScope.snapChatPacket = {};
  $rootScope.smsPacket = {};
  $rootScope.emailPacket = {};

  $ionicPopover.fromTemplateUrl('templates/docker-popover.html', {
	scope: $rootScope
  }).then(function(popover) {
		$rootScope.dockerPopover = popover;
	});

  $rootScope.openDockerPopover = function($event) {
	$rootScope.popoverOpen = 'Docker';
	$rootScope.data.shippingSection = 'transmissionSelect';
	$rootScope.data.outType = '';
	$rootScope.data.currency = '';
	$rootScope.data.gatewaySelected = {};
	$rootScope.dockerPopover.show($event);
  }

  $rootScope.closeDocker = function() {
    $rootScope.dockerPopover.hide();
  }

  $rootScope.closeAnchor = function() {
	$rootScope.anchorPopover.hide();
  }

  $rootScope.toggleChat = function(){
	if ($rootScope.chatData.chatOn){
		$rootScope.chatData.chatOn = false;
	}
	else {
		$rootScope.chatData.chatOn = true;
		accountServices.getRootTransactions();
	}
  }

  $localForage.getItem('lastTransactions').then(function(data) {
  	if (!data){
		$localForage.setItem('lastTransactions', JSON.stringify([])).then(function(data) {

		});
	}
  });

  accountServices.getRootTransactions();
  accountServices.getRippleTxtSettings();

  $rootScope.$watch('packet.rippleName', _.debounce(function (newValue, oldValue) {
	if (newValue){
		$http.get('https://id.ripple.com/v1/user/' + newValue).
			success(function(data, status, headers, config) {
				if (data && data.address){
					$rootScope.blob = data;
				}
				else {
					$rootScope.blob = '';
				}
			}).
			error(function(data, status, headers, config) {
				$rootScope.blob = '';
			});
	}
	else {
		$rootScope.blob = '';
	}
  }, 500), true);

  try {
	accountServices.messageSubscribe();
  }
  catch(err) {
  }

  $rootScope.sendMessage = function(){
	if ($rootScope.chatData.message && $rootScope.loggedInAccount.publicKey){
		$rootScope.chatData.sending = true;
		var amount = ripple.Amount.from_human('0.1XRP');
		var rippleFunc = function(){
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: amount
			});

			transaction.addMemo('Provichat', JSON.stringify({type : 'chatRoom', rooms : ['provichat']}));

			transaction.addMemo('message', $rootScope.chatData.message.toString());

			transaction.submit(function(err, res) {
				$rootScope.$apply(function(){
					if (!err){
						var message = {};
						message.message = $sce.trustAsHtml($sanitize(linkify.twitterRippleProvidence(angular.copy($rootScope.chatData.message))));
						message.date = res.tx_json.date;
						message.hash = res.tx_json.hash;
						message.address = res.tx_json.Account;
						message.username = $rootScope.loggedInAccount.rippleName;
						var inRay = _.find($rootScope.chatData.messages, function(sigged) {return message.hash == sigged.hash;});
						if (!inRay){
							$rootScope.chatData.messages.unshift(message);
						}
					}
					$rootScope.chatData.message = '';
					$rootScope.chatData.sending = false;
				});
			});
		}
		if ($rootScope.remote.isConnected()){
			rippleFunc();
		}
		else {
			$rootScope.remote.connect(function() {
				rippleFunc();
			});
		}
	}
  }

  $rootScope.switchShippingSection = function(item){
	  $rootScope.data.shippingSection = item;
  }

  $rootScope.requestToken = function(){
	  var BlobClient = new ripple.VaultClient();
	  BlobClient.requestToken($rootScope.loggedInAccount.twoFactor.blob_url, $rootScope.loggedInAccount.twoFactor.blob_id, 1, function(){

	  });
  }

  $rootScope.redeem = function(){
	  var defer = $q.defer();
	  if ($rootScope.packet.redeemCode || $rootScope.redeemCode){
		  $http.post('https://api.pier.exchange/amountLookup', {code : $rootScope.packet.redeemCode || $rootScope.redeemCode}).
			  success(function(data, status, headers, config) {
				  if (data.transactionHash){

					  // TODO : if funded or not

					  var rippleFunc = function(){
						  $rootScope.remote.requestTransaction(data.transactionHash, function(err,transaction){
							  if (transaction && transaction.Amount && typeof transaction.Amount === 'object'){
								  var trustLevel;
								  if ($rootScope.loggedInAccount.accountLines && $rootScope.loggedInAccount.accountLines[0]){
									  $rootScope.loggedInAccount.accountLines.forEach(function(inf){
										  if (inf.currency == transaction.Amount.currency && inf.issuer == transaction.Amount.issuer && Number(transaction.Amount.value)){

										  }
									  });
								  }
								  if (!trustLevel){
									  $rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

									  var transaction = $rootScope.remote.createTransaction('TrustSet', {
										  account: $rootScope.loggedInAccount.publicKey,
										  limit: '1000000000000/' + transaction.Amount.currency + '/' + transaction.Amount.issuer
									  });

									  transaction.submit(function(err, res) {
										  $http.post('https://api.pier.exchange/redeem', {code : $rootScope.redeemCode, rippleAddress : $rootScope.loggedInAccount.publicKey}).
											  success(function(data, status, headers, config) {
												  defer.resolve(true);
											  }).
											  error(function(data, status, headers, config) {
												  defer.resolve(false);
											  });
									  });
								  }
								  else {
									  $http.post('https://api.pier.exchange/redeem', {code : $rootScope.redeemCode, rippleAddress : $rootScope.loggedInAccount.publicKey}).
										  success(function(data, status, headers, config) {
											  defer.resolve(true);
										  }).
										  error(function(data, status, headers, config) {
											  defer.resolve(false);
										  });
								  }
							  }
							  else if (transaction && !isNaN(transaction.Amount)){
								  $http.post('https://api.pier.exchange/redeem', {code : $rootScope.redeemCode, rippleAddress : $rootScope.loggedInAccount.publicKey}).
									  success(function(data, status, headers, config) {
										  defer.resolve(true);
									  }).
									  error(function(data, status, headers, config) {
										  defer.resolve(false);
									  });
							  }
						  });
					  }
					  if ($rootScope.remote.isConnected()){
						  rippleFunc();
					  }
					  else {
						  $rootScope.remote.connect(function() {
							  rippleFunc();
						  });
					  }
				  }
			  }).
			  error(function(data, status, headers, config) {
				  defer.resolve(false);
			  });
	  }
	  return defer.promise;
  }

  $rootScope.login = function(){
	  var defer = $q.defer();
	  var VC = new ripple.VaultClient();
	  VC.loginAndUnlock($rootScope.packet.rippleName, $rootScope.packet.password, '34535345', function(err,info){
		  $rootScope.loggedInAccount.connecting = false;
		  $rootScope.swirl = false;
		  if (err && err.twofactor){
			  $rootScope.$apply(function(){
				  $rootScope.loggedInAccount.twoFactor = err.twofactor;
			  });
		  }
		  else if (!err && info){
			  $rootScope.disconnectSocial().then(function(){

			  });
			  $rootScope.loggedInAccount = {};
			  $rootScope.loggedInAccount.rippleName = info.username;
			  $rootScope.loggedInAccount.publicKey = info.blob.data.account_id;
			  $rootScope.loggedInAccount.email = info.blob.data.email;
			  $rootScope.loggedInAccount.secretKey = info.secret;
			  $rootScope.redeem();
			  $rootScope.blob.secret = info;
			  $localForage.getItem('profile').then(function(data) {
				  if (data){
					  var datum = JSON.parse(data);
					  if (datum && datum[0]){
						  datum.forEach(function(dat){
							  if (dat.rippleName == $rootScope.loggedInAccount.rippleName){
								  $rootScope.info.name = dat.name;
								  $rootScope.info.email = dat.email;
							  }
						  });
					  }
				  }
			  });
			  if ($rootScope.pledge && $rootScope.pledge.admins){
				  $rootScope.pledge.admins.forEach(function(admin){
					  if (admin.rippleAddress ==  $rootScope.loggedInAccount.publicKey){
						  $rootScope.admin = admin;
					  }
				  });
			  }
			  var rippleFunc = function(){
				  accountServices.getInfo();
				  $rootScope.remote.requestUnsubscribe(['transactions'], function(){
					  var request = $rootScope.remote.requestSubscribe(['transactions']);
					  request.setServer($rootScope.rippled);

					  $rootScope.remote.on('transaction', function onTransaction(transaction) {
						  accountServices.accountInfo(transaction);
						  accountServices.messageInfo(transaction);
					  });
					  request.request();
				  });
			  }
			  if ($rootScope.remote.isConnected()){
				  rippleFunc();
			  }
			  else {
				  $rootScope.remote.connect(function() {
					  rippleFunc();
				  });
			  }
			  defer.resolve();
		  }
	  });
	  return defer.promise;
  }

  $rootScope.openRedeem = function(){
	  var myPopup = $ionicPopup.show({
		  template: '<input type="text" placeholder="Redeem Code" class="pier-primary" ng-model="packet.redeemCode" style="padding:5px;color: #14274d;">',
		  title: '<span class="pier-primary ionHead">Redeem</span>',
		  scope: $rootScope,
		  buttons: [
			  {
				  text: '<span class="pier-primary smallButton">Cancel</span>',
				  type: 'marketing-secondary-bg'
			  },
			  {
				  text: '<span class="pier-primary smallButton">Redeem</span>',
				  type: 'marketing-primary-bg',
				  onTap: function(e) {
					  e.preventDefault();
					  $rootScope.redeem.then(function(truth){
						  $rootScope.packet.redeemCode = '';
						  if (truth){
							  myPopup.close();
						  }
					  });
				  }
			  }
		  ]
	  });
  }

  $rootScope.connectRipplePrompt = function(){
	var deferred = $q.defer();
	var myPopup = $ionicPopup.show({
		template: '<span class="pier-primary" style="position: absolute;text-align: center;left: 50%;top: 55px;margin-left: -7px;" ng-if="loggedInAccount.connecting"><i class="icon ion-loading-c" style="height: 14px;width: 13px;"></i></span><input type="text" placeholder="Ripple Name" class="pier-primary" ng-model="packet.rippleName" style="padding:5px;color: #14274d;"><i ng-if="blob" class="icon ion-checkmark-circled" style="position: absolute;top: 88px;left: 100%;margin-left: -40px;font-size: 20px;background: #fff;width: 30px;padding-left: 10px;color: #14274d;"></i><input type="password" class="pier-primary" placeholder="Password" style="padding:5px;color: #14274d;" ng-model="packet.password"><input ng-if="loggedInAccount.twoFactor" type="text" class="pier-primary" placeholder="Token" style="padding:5px;color: #14274d;" ng-model="packet.twofactor"><button ng-click="requestToken()" style="text-align:center;margin-top:10px;" ng-if="loggedInAccount.twoFactor"><span class="pier-primary smallButton marketing-primary-bg">SMS Token</span></button>',
		title: '<span class="pier-primary ionHead">Connect Ripple</span>',
		subTitle: '<a target="_blank" href="https://rippletrade.com" class="pier-primary">Have a Ripple account?</span>',
		scope: $rootScope,
		buttons: [
			{
				text: '<span class="pier-primary smallButton">Cancel</span>',
				type: 'marketing-secondary-bg'
			},
			{
				text: '<span class="pier-primary smallButton">Connect</span>',
				type: 'marketing-primary-bg',
				onTap: function(e) {
					if (!$rootScope.packet.rippleName || !$rootScope.packet.password) {
						e.preventDefault();
					}
					else {
						$rootScope.swirl = true;
						$rootScope.loggedInAccount.connecting = true;
						e.preventDefault();
						if ($rootScope.loggedInAccount.twoFactor){
							var options = {
								url         : $rootScope.loggedInAccount.twoFactor.blob_url,
								id          : $rootScope.loggedInAccount.twoFactor.blob_id,
								device_id   : $rootScope.loggedInAccount.twoFactor.device_id,
								token       : $rootScope.packet.twofactor,
								remember_me : true
							};
							new ripple.VaultClient().verifyToken(options, function(err, resp) {
								if (err) {

								} else {
									$rootScope.login().then(function(){
										deferred.resolve();
										myPopup.close();
									});
								}
							});
							$rootScope.loggedInAccount.twoFactor;
						}
						else {
							$rootScope.login().then(function(){
								deferred.resolve();
								myPopup.close();
							});
						}
					}
				}
			}
		]
	});
	return deferred.promise;
  }

  function isValidUrl(url) {
	return url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
  }

  function anchorRegister(){
	  var poster = function(objectReqNew, defer){
		  $http.post('https://api.pier.exchange/applyForAnchor', objectReqNew).
			  success(function(data, status, headers, config) {
				  $rootScope.gatewayInfo.currency = '';
				  defer.resolve(true);
			  }).
			  error(function(data, status, headers, config) {

			  });
	  }
	  if ($rootScope.gatewayInfo.name && $rootScope.gatewayInfo.domain && isValidUrl($rootScope.gatewayInfo.domain) && typeof $rootScope.gatewayInfo.currency == 'string' && $rootScope.gatewayInfo.currency.length == 3 && $rootScope.loggedInAccount.email){
		  $rootScope.swirl = true;
		  accountServices.noNetNeutrality({currency : $rootScope.gatewayInfo.currency, domained : $rootScope.gatewayInfo.domain, name : $rootScope.gatewayInfo.name, email : $rootScope.loggedInAccount.email}, poster).then(function(){
			  $rootScope.swirl = false;
		  });
	  }
  }

  // TODO : save in and retrieve from localstorage from fields

  $rootScope.$watchCollection('[data.amountCurrency, loggedInAccount.email, snapChatPacket.username, snapChatPacket.password, snapChatPacket.recipients, snapChatPacket.valid, gatewayInfo.name, gatewayInfo.domain, gatewayInfo.currency, emailPacket.recipients, smsPacket.from, smsPacket.recipients, smsPacket.message, linkedinPacket.dm, linkedinPacket.recipients, facebookPacket.dm, facebookPacket.recipients, data.outType, data.linkedinConnected, data.facebookConnected, data.twitterConnected, twitterPacket.dm, twitterPacket.recipients]', function(newValues){
	  if (!isNaN(Number($rootScope.data.amountCurrency))){
		  if ($rootScope.snapChatPacket && $rootScope.snapChatPacket.username && $rootScope.snapChatPacket.password && $rootScope.snapChatPacket.recipients && $rootScope.snapChatPacket.recipients[0] && $rootScope.snapChatPacket.valid){
			  $rootScope.data.validSnapchatFields = true;
		  }
		  else {
			  $rootScope.data.validSnapchatFields = false;
		  }
		  if ($rootScope.linkedinPacket && $rootScope.linkedinPacket.dm){
			  if ($rootScope.data.linkedinConnected && $rootScope.linkedinPacket.recipients && $rootScope.linkedinPacket.recipients[0]){
				  $rootScope.data.validLinkedinFields = true;
			  }
			  else {
				  $rootScope.data.validLinkedinFields = false;
			  }
		  }
		  else if ($rootScope.linkedinPacket){
			  if ($rootScope.data.linkedinConnected){
				  $rootScope.data.validLinkedinFields = true;
			  }
			  else {
				  $rootScope.data.validLinkedinFields = false;
			  }
		  }
		  if ($rootScope.facebookPacket && $rootScope.facebookPacket.dm){
			  if ($rootScope.data.facebookConnected && $rootScope.facebookPacket.recipients && $rootScope.facebookPacket.recipients[0]){
				  $rootScope.data.validFacebookFields = true;
			  }
			  else {
				  $rootScope.data.validFacebookFields = false;
			  }
		  }
		  else if ($rootScope.facebookPacket){
			  if ($rootScope.data.facebookConnected){
				  $rootScope.data.validFacebookFields = true;
			  }
			  else {
				  $rootScope.data.validFacebookFields = false;
			  }
		  }
		  if ($rootScope.twitterPacket && $rootScope.twitterPacket.dm){
			  if ($rootScope.data.twitterConnected && $rootScope.twitterPacket.recipients && $rootScope.twitterPacket.recipients[0]){
				  $rootScope.data.validTwitterFields = true;
			  }
			  else {
				  $rootScope.data.validTwitterFields = false;
			  }
		  }
		  else if ($rootScope.twitterPacket){
			  if ($rootScope.data.twitterConnected){
				  $rootScope.data.validTwitterFields = true;
			  }
			  else {
				  $rootScope.data.validTwitterFields = false;
			  }
		  }
		  if ($rootScope.smsPacket && $rootScope.smsPacket.from && $rootScope.smsPacket.message && $rootScope.smsPacket.recipients && $rootScope.smsPacket.recipients[0]){
			  var notValid;
			  $rootScope.smsPacket.recipients.forEach(function(recipient){
				  if (!accountServices.validateSMS(recipient.text)){
					  notValid = true;
				  }
			  });
			  if (!notValid){
				  $rootScope.data.validSMSFields = true;
			  }
			  else {
				  $rootScope.data.validSMSFields = false;
			  }
		  }
		  else {
			  $rootScope.data.validSMSFields = false;
		  }
		  if ($rootScope.emailPacket && $rootScope.loggedInAccount.name && $rootScope.loggedInAccount.email && accountServices.validateEmail($rootScope.loggedInAccount.email) && $rootScope.emailPacket.recipients && $rootScope.emailPacket.recipients[0]){
			  var notValid;
			  $rootScope.emailPacket.recipients.forEach(function(recipient){
			  	  if (!accountServices.validateEmail(recipient.text)){
					  notValid = true;
				  }
			  });
			  if (!notValid){
				  $rootScope.data.validEmailFields = true;
			  }
			  else {
				  $rootScope.data.validEmailFields = false;
			  }
		  }
		  else {
			  $rootScope.data.validEmailFields = false;
		  }
	  }
	  if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.email && accountServices.validateEmail($rootScope.loggedInAccount.email) && $rootScope.gatewayInfo.name && $rootScope.gatewayInfo.domain && accountServices.isValidUrl($rootScope.gatewayInfo.domain) && $rootScope.gatewayInfo.name && $rootScope.gatewayInfo.currency && $rootScope.gatewayInfo.currency.length == 3){
		  $rootScope.data.anchorFieldsValid = true;
		  $rootScope.data.dockerFieldsValid = true;
	  }
	  else {
		  $rootScope.data.anchorFieldsValid = false;
		  $rootScope.data.dockerFieldsValid = false;
	  }
  });

  $rootScope.shipFacebook = function(path){
	if ($rootScope.data.validFacebookFields){
		$rootScope.swirl = true;
		var parseRecipients = '';
		var counter = 1;
		var subtype;
		if ($rootScope.facebookPacket.dm){
			subtype = 'dm';
		}
		else {
			subtype = 'upForGrabs';
		}
		if ($rootScope.facebookPacket.dm){
			$rootScope.facebookPacket.recipients.forEach(function(text){
				if (counter != $rootScope.facebookPacket.recipients.length){
					parseRecipients += text.realid + ' ';
				}
				else {
					parseRecipients += text.realid;
				}
				counter += 1;
			});
		}
		var poster = function(objectReqNew, defer){
			$http.post('https://api.pier.exchange/facebook', objectReqNew).
				success(function(data, status, headers, config) {
					accountServices.sendTransaction(path, 'facebook', data.dt).then(function(hash){
						if ($rootScope.popoverOpen == 'Anchor'){
							$rootScope.data.shippingSection = 'initial';
							$rootScope.anchorPopover.hide();
						}
						else {
							$rootScope.data.shippingSection = 'initial';
							$rootScope.dockerPopover.hide();
						}
						$rootScope.facebookPacket.recipients = [];
						$rootScope.data.currency = '';
						$rootScope.data.selectedGateway = {};
						$rootScope.data.outType = '';
						$rootScope.data.shippingSection = 'transmissionSelect';
						$rootScope.data.section = 'ship';
						$rootScope.swirl = false;
					});
				}).
				error(function(data, status, headers, config) {
					$rootScope.swirl = false;
				});
		}
		accountServices.noNetNeutrality({recipients : parseRecipients, subtype : subtype, currency : $rootScope.data.currency, issuer : $rootScope.data.selectedGateway.rippleAddress}, poster).then(function(){

		});
	}
  }

  $rootScope.shipTwitter = function(path){
	if ($rootScope.data.validTwitterFields){
		$rootScope.swirl = true;
		var parseRecipients = '';
		var counter = 1;
		var subtype;
		if ($rootScope.twitterPacket.dm){
			subtype = 'dm';
		}
		else {
			subtype = 'upForGrabs';
		}
		if ($rootScope.twitterPacket.dm){
			$rootScope.twitterPacket.recipients.forEach(function(text){
				if (counter != $rootScope.twitterPacket.recipients.length){
					parseRecipients += text.realid + ' ';
				}
				else {
					parseRecipients += text.realid;
				}
				counter += 1;
			});
		}
		var poster = function(objectReqNew, defer){
			$http.post('https://api.pier.exchange/twitter', objectReqNew).
				success(function(data, status, headers, config) {
					accountServices.sendTransaction(path, 'twitter', data.dt).then(function(hash){
						if ($rootScope.popoverOpen == 'Anchor'){
							$rootScope.data.shippingSection = 'initial';
							$rootScope.anchorPopover.hide();
						}
						else {
							$rootScope.data.shippingSection = 'initial';
							$rootScope.dockerPopover.hide();
						}
						$rootScope.twitterPacket.recipients = [];
						$rootScope.data.currency = '';
						$rootScope.data.selectedGateway = {};
						$rootScope.data.outType = '';
						$rootScope.data.shippingSection = 'transmissionSelect';
						$rootScope.data.section = 'ship';
						$rootScope.swirl = false;
					});
				}).
				error(function(data, status, headers, config) {
					$rootScope.swirl = false;
				});
		}
		accountServices.noNetNeutrality({recipients : parseRecipients, subtype : subtype, currency : $rootScope.data.currency, issuer : $rootScope.data.selectedGateway.rippleAddress}, poster).then(function(){

		});
	}
  }

  $rootScope.shipLinkedin = function(path){
	if ($rootScope.data.validLinkedinFields){
		$rootScope.swirl = true;
		var parseRecipients = '';
		var counter = 1;
		var subtype;
		if ($rootScope.linkedinPacket.dm){
			subtype = 'dm';
		}
		else {
			subtype = 'upForGrabs';
		}
		if ($rootScope.linkedinPacket.dm){
			$rootScope.linkedinPacket.recipients.forEach(function(text){
				if (counter != $rootScope.linkedinPacket.recipients.length){
					parseRecipients += text.realid + ' ';
				}
				else {
					parseRecipients += text.realid;
				}
				counter += 1;
			});
		}
		var poster = function(objectReqNew, defer){
			$http.post('https://api.pier.exchange/linkedin', objectReqNew).
				success(function(data, status, headers, config) {
					accountServices.sendTransaction(path, 'linkedin', data.dt).then(function(hash){
						if ($rootScope.popoverOpen == 'Anchor'){
							$rootScope.data.shippingSection = 'initial';
							$rootScope.anchorPopover.hide();
						}
						else {
							$rootScope.data.shippingSection = 'initial';
							$rootScope.dockerPopover.hide();
						}
						$rootScope.linkedinPacket.recipients = [];
						$rootScope.data.currency = '';
						$rootScope.data.selectedGateway = {};
						$rootScope.data.outType = '';
						$rootScope.data.shippingSection = 'transmissionSelect';
						$rootScope.data.section = 'ship';
						$rootScope.swirl = false;
					});
				}).
				error(function(data, status, headers, config) {
					$rootScope.swirl = false;
				});
		}
		accountServices.noNetNeutrality({recipients : parseRecipients, subtype : subtype, currency : $rootScope.data.currency, issuer : $rootScope.data.selectedGateway.rippleAddress}, poster).then(function(){

		});
	}
  }

  $rootScope.shipEmail = function(path){
	  if ($rootScope.data.validEmailFields){
		  $rootScope.swirl = true;
		  var parseRecipients = '';
		  var counter = 1;
		  $rootScope.emailPacket.recipients.forEach(function(text){
			  if (counter != $rootScope.emailPacket.recipients.length){
				  parseRecipients += text.realid + ' ';
			  }
			  else {
				  parseRecipients += text.realid;
			  }
			  counter += 1;
		  });
		  var poster = function(objectReqNew, defer){
			  $http.post('https://api.pier.exchange/email', objectReqNew).
				  success(function(data, status, headers, config) {
					  accountServices.sendTransaction(path, 'email', data.dt).then(function(hash){
						  if ($rootScope.popoverOpen == 'Anchor'){
							  $rootScope.data.shippingSection = 'initial';
							  $rootScope.anchorPopover.hide();
						  }
						  else {
							  $rootScope.data.shippingSection = 'initial';
							  $rootScope.dockerPopover.hide();
						  }
						  $rootScope.emailPacket.recipients = [];
						  $rootScope.data.currency = '';
						  $rootScope.data.selectedGateway = {};
						  $rootScope.data.outType = '';
						  $rootScope.data.shippingSection = 'transmissionSelect';
						  $rootScope.data.section = 'ship';
						  $rootScope.swirl = false;
					  });
				  }).
				  error(function(data, status, headers, config) {
					  $rootScope.swirl = false;
				  });
		  }
		  accountServices.noNetNeutrality({name : $rootScope.loggedInAccount.name, email : $rootScope.loggedInAccount.email, recipients : parseRecipients, currency : $rootScope.data.currency, issuer : $rootScope.data.selectedGateway.rippleAddress}, poster).then(function(){

		  });
	  }
  }

  $rootScope.shipSms = function(path){
	  if ($rootScope.data.validSMSFields){
		  $rootScope.swirl = true;
		  var parseRecipients = '';
		  var counter = 1;
		  $rootScope.smsPacket.recipients.forEach(function(text){
			  if (counter != $rootScope.smsPacket.recipients.length){
				  parseRecipients += text.realid + ' ';
			  }
			  else {
				  parseRecipients += text.realid;
			  }
			  counter += 1;
		  });
		  var poster = function(objectReqNew, defer){
			  $http.post('https://api.pier.exchange/sms', objectReqNew).
				  success(function(data, status, headers, config) {
					  accountServices.sendTransaction(path, 'sms', data.dt).then(function(hash){
						  if ($rootScope.popoverOpen == 'Anchor'){
							  $rootScope.data.shippingSection = 'initial';
							  $rootScope.anchorPopover.hide();
						  }
						  else {
							  $rootScope.data.shippingSection = 'initial';
							  $rootScope.dockerPopover.hide();
						  }
						  $rootScope.smsPacket.recipients = [];
						  $rootScope.data.currency = '';
						  $rootScope.data.selectedGateway = {};
						  $rootScope.smsPacket.message = '';
						  $rootScope.data.outType = '';
						  $rootScope.data.shippingSection = 'transmissionSelect';
						  $rootScope.data.section = 'ship';
						  $rootScope.swirl = false;
					  });
				  }).
				  error(function(data, status, headers, config) {
					  $rootScope.swirl = false;
				  });
		  }
		  accountServices.noNetNeutrality({from : $rootScope.smsPacket.from, message : $rootScope.smsPacket.message, recipients : parseRecipients, currency : $rootScope.data.currency, issuer : $rootScope.data.selectedGateway.rippleAddress}, poster).then(function(){

		  });
	  }
  }

  $rootScope.shipSnapchat = function(path){
      if ($rootScope.data.validSnapchatFields){
		  $rootScope.swirl = true;
		  var parseRecipients = '';
		  var counter = 1;
		  $rootScope.snapChatPacket.recipients.forEach(function(text){
			  if (counter != $rootScope.snapChatPacket.recipients.length){
				  parseRecipients += text.realid + ' ';
			  }
			  else {
				  parseRecipients += text.realid;
			  }
			  counter += 1;
		  });
		  var poster = function(objectReqNew, defer){
			  $http.post('https://api.pier.exchange/snapchat', objectReqNew).
				  success(function(data, status, headers, config) {
					  accountServices.sendTransaction(path, 'snapchat', data.dt).then(function(hash){
						  if ($rootScope.popoverOpen == 'Anchor'){
							  $rootScope.data.shippingSection = 'initial';
							  $rootScope.anchorPopover.hide();
						  }
						  else {
							  $rootScope.data.shippingSection = 'initial';
							  $rootScope.dockerPopover.hide();
						  }
						  $rootScope.snapChatPacket.recipients = [];
						  $rootScope.data.currency = '';
						  $rootScope.data.selectedGateway = {};
						  $rootScope.data.outType = '';
						  $rootScope.data.shippingSection = 'transmissionSelect';
						  $rootScope.data.section = 'ship';
						  $rootScope.swirl = false;
					  });
				  }).
				  error(function(data, status, headers, config) {
					  $rootScope.swirl = false;
				  });
		  }
		  accountServices.noNetNeutrality({username : $rootScope.snapChatPacket.username, password : $rootScope.snapChatPacket.password, explainer : $rootScope.snapChatPacket.explainer, recipients : parseRecipients, currency : $rootScope.data.currency, issuer : $rootScope.data.selectedGateway.rippleAddress}, poster).then(function(){

		  });
	  }
  }

  $rootScope.dockerRegister = function(){
	  var poster = function(objectReqNew, defer){
		  $http.post('https://api.pier.exchange/registerDocker', objectReqNew).
			  success(function(data, status, headers, config) {
				  $rootScope.gatewayInfo.currency = '';
				  defer.resolve(true);
			  }).
			  error(function(data, status, headers, config) {

			  });
	  }
	  if ($rootScope.gatewayInfo.name && $rootScope.gatewayInfo.domain && isValidUrl($rootScope.gatewayInfo.domain) && typeof $rootScope.gatewayInfo.currency == 'string' && $rootScope.gatewayInfo.currency.length == 3 && $rootScope.loggedInAccount.email){
		  $rootScope.swirl = true;
		  accountServices.noNetNeutrality({currency : $rootScope.gatewayInfo.currency, domained : $rootScope.gatewayInfo.domain, name : $rootScope.gatewayInfo.name, email : $rootScope.loggedInAccount.email}, poster).then(function(){
			  $rootScope.swirl = false;
		  });
	  }
  }

  moment.locale('en', {
	relativeTime : {
		future: "IN %s",
		past:   "%s AGO",
		s:  "SECONDS",
		m:  "ONE MINUTE",
		mm: "%d MINUTES",
		h:  "ONE HOUR",
		hh: "%h HOURS",
		d:  "ONE DAY",
		dd: "%d DAYS",
		M:  "ONE MONTH",
		MM: "%d MONTHS",
		y:  "ONE YEAR",
		yy: "%d YEARS"
	}
  });

  $rootScope.getAvailable = function(){
	  var rippleFunc = function(){
		  $rootScope.remote.request('account_offers', $rootScope.receiveAddress, function(err, info) {
			  if (info && info.offers && info.offers[0]){
				  var offerTotal = 0;
				  info.offers.forEach(function(offer){
					  if (offer.taker_gets.currency == 'PVD'){
						  offerTotal += Number(offer.taker_gets.value);
					  }
				  });
				  $rootScope.sharesAvailable = offerTotal;
			  }
		  });
	  }
	  if ($rootScope.remote.isConnected()){
		  rippleFunc();
	  }
	  else {
		  $rootScope.remote.connect(function() {
			  rippleFunc();
		  });
	  }
  }

  // TODO : set navigation settings back to default on major action

  $rootScope.selectTransmissionMethod = function(item){
	$rootScope.data.outType = item.name;
	$rootScope.data.shippingSection = 'gatewaySelect';
  }

  $rootScope.selectGateway = function(currency, gateway){
	  $rootScope.data.pathObject = {};
	  $rootScope.data.amountCurrency = '';
	  $rootScope.data.currency = currency;
	  $rootScope.data.selectedGateway = gateway;
	  $rootScope.data.shippingSection = 'final';
  }

  function pathFind(newValue){
	var rippleFunc = function(){
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
		if (!isNaN(Number(newValue)) && Number(newValue) > 0){
			var sourceCurrencies = [{"currency" : "XRP"}];
			if ($rootScope.loggedInAccount.accountLines && $rootScope.loggedInAccount.accountLines[0]){
				$rootScope.loggedInAccount.accountLines.forEach(function(inf){
					sourceCurrencies.push({"currency" : inf.currency});
				});
			}
			pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.publicKey, $rootScope.receiveAddress, {"currency": $rootScope.data.currency, "value": newValue.toString(), "issuer": $rootScope.data.selectedGateway.rippleAddress}, sourceCurrencies, function(err,real){
				$rootScope.$apply(function(){
					$rootScope.data.pathObject = real;
				});
			});
			pf.request();
			pf.on('update', function (upd) {
				$rootScope.$apply(function(){
					$rootScope.data.pathObject = upd;
				});
			});
		}
		else {
			$rootScope.data.pathObject = {};
		}
	}
	if ($rootScope.remote.isConnected()){
		rippleFunc();
	}
	else {
		$rootScope.remote.connect(function() {
			rippleFunc();
		});
	}
  }

  var lineWatch = $rootScope.$watch('data.selectedGateway', function(newValue, oldValue){
	if ($rootScope.data.amountCurrency){
		pathFind($rootScope.data.amountCurrency);
	}
  });

  $rootScope.$watchCollection('[data.currency, data.gatewaySelected]', _.debounce(function (newValue, oldValue) {
	  if ($rootScope.popoverOpen == 'Docker' && $rootScope.data.currency && $rootScope.data.currency.length == 3){
  		  pathFind(newValue);
	  }
  }, 500), true);

  $rootScope.$watch('data.amountCurrency', _.debounce(function (newValue, oldValue) {
	  if (!isNaN(newValue)){
		  pathFind(newValue);
	  }
	  else {
		  $rootScope.data.pathObject = {};
	  }
  }, 500), true);

  $rootScope.$watchCollection('[data.gatewaySelected.input, data.currency]', _.debounce(function (newValue, oldValue) {
	  if ($rootScope.data.gatewaySelected.input && !isNaN(Number($rootScope.data.currency)) && $rootScope.data.currency.length == 3){
		  $http.post('https://api.pier.exchange/isApproved', {rippleName : $rootScope.data.gatewaySelected.input, currency : $rootScope.data.currency}).
			  success(function(data, status, headers, config) {
				if (data.success){
					$rootScope.data.gatewaySelected = data.gateway;
					$rootScope.data.shippingSection = 'final';
				}
			  }).
			  error(function(data, status, headers, config) {

			  });
	  }
  }, 500), true);

  $rootScope.$watch('snapChatPacket', _.debounce(function (newValue, oldValue) {
  	if (newValue.username && newValue.password){
		$http.post('https://api.pier.exchange/snapchatConfirm', {username : newValue.username, password : newValue.password}).
			success(function(data, status, headers, config) {
				$rootScope.snapChatPacket.friends = data.friends || [];
				if ($rootScope.snapChatPacket.friends && $rootScope.snapChatPacket.friends[0]){
					$rootScope.snapChatPacket.friends.forEach(function(friend){
						snapSearch.add(friend);
					});
				}
				if (data.success){
					$rootScope.snapChatPacket.valid = true;
				}
				else {
					$rootScope.snapChatPacket.valid = false;
				}
			}).
			error(function(data, status, headers, config) {
				$rootScope.snapChatPacket.valid = false;
			});
	}
	else {
		$rootScope.snapChatPacket.valid = false;
	}
  }, 500), true);

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}])

.config(['$stateProvider', '$urlRouterProvider', '$compileProvider', function($stateProvider, $urlRouterProvider, $compileProvider) {
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|^\s*data:image\//);
  $stateProvider
	.state('root', {
	  url: '/:code',
	  views: {
		  'tab-pledge': {
			  controller: 'CodeCtrl'
		  }
	  },
	  resolve: {
			getParams: ['$q', '$stateParams', '$rootScope', 'accountServices', '$http', function($q, $stateParams, $rootScope, accountServices, $http){
				var defer = $q.defer();
				$rootScope.redeemCode = $stateParams.code;
				defer.resolve();
				return defer.promise;
			}]
		}
	})

  $urlRouterProvider.otherwise('/');

}]);

