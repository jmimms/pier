angular.module('starter.services', [])

.factory('accountServices', ['$rootScope', '$http', '$q', '$sce', '$localForage', '$sanitize', 'linkify', function($rootScope, $http, $q, $sce, $localForage, $sanitize, linkify) {
	var accountFunctions = {};
	accountFunctions.offset = 0;
	accountFunctions.twitter = function(){
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
	}
	accountFunctions.getRippleTxtSettings = function(){
		var defer = $q.defer();
		$http.get($rootScope.rippleTxtUrl)
			.success(function(data, status, headers, config) {
				if (data){
					var settings = accountFunctions.decodered(data);
					if (settings.request_fees){
						settings.request_fees = Object.getOwnPropertyNames(settings.request_fees)[0];
					}
					if (settings.request_time){
						settings.request_time = Object.getOwnPropertyNames(settings.request_time)[0];
					}
					$rootScope.settings = settings;
					if ($rootScope.settings.request_fees){
						if (typeof $rootScope.settings.request_fees == 'string'){
							$rootScope.settings.request_fees = $rootScope.settings.request_fees.split(',');
						}
					}
					defer.resolve($rootScope.settings);
				}
			}).
			error(function(data, status, headers, config) {

			});
		return defer.promise;
	}
	accountFunctions.noNetNeutrality = function(objectReq, poster){
		var defer = $q.defer();
		function callTransact(){
			var feePaid;
			objectReq.rippleAddress = $rootScope.loggedInAccount.publicKey;
			objectReq.rippleName = $rootScope.loggedInAccount.rippleName;

			$localForage.getItem('lastTransactions').then(function(data) {
				var transactionList = JSON.parse(data);
				if (transactionList && transactionList[0] && $rootScope.settings.request_time && !isNaN(Number($rootScope.settings.request_time))){
					var today = new Date();
					transactionList.forEach(function(last){
						if (last.domain == $rootScope.domain){
							var date = new Date((Number(last.dateTime) + 946684800) * 1000),
								timeRefine = Number((Number($rootScope.settings.request_time) * 100).toFixed()),
								todayPlusMinute = new Date(today.getTime() + timeRefine),
								todayMinusMinute = new Date(today.getTime() - timeRefine);
							if (date <= todayPlusMinute && date >= todayMinusMinute){
								feePaid = true;
								objectReq.hash = last.hash;
								objectReq.signedDateTime = ripple.Message.signMessage(last.dateTime.toString(), $rootScope.loggedInAccount.secretKey, objectReq.rippleAddress);
							}
						}
					});
				}
				if (feePaid){
					poster(objectReq, defer);
				}
				else {
					var amount = $rootScope.selectedFee;
					if (!amount && $rootScope.settings && $rootScope.settings.request_fees && $rootScope.settings.request_fees[0]){
						amount = $rootScope.settings.request_fees[0];
					}
					var rippleFunc = function(){
						$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);
						var transaction = $rootScope.remote.createTransaction('Payment', {
							account: $rootScope.loggedInAccount.publicKey,
							destination: $rootScope.receiveAddress,
							amount: amount
						});
						transaction.addMemo('request', '', $rootScope.loggedInAccount.secretKey, objectReq.rippleAddress);

						transaction.submit(function(err, res) {
							if (!err && res && res.tx_json && res.tx_json.hash){
								objectReq.signedDateTime = ripple.Message.signMessage(res.tx_json.date.toString(), $rootScope.loggedInAccount.secretKey, objectReq.rippleAddress);
								objectReq.backupHash = res.tx_json.hash;
								objectReq.hash = res.tx_json.hash;
								$localForage.getItem('lastTransactions').then(function(data) {
									var transactionList = JSON.parse(data);
									var inList;
									if (transactionList && transactionList[0]){
										transactionList = _.map(transactionList, function(item){if (item.domain == $rootScope.domain){inList = true; item = {hash : res.tx_json.hash, dateTime: res.tx_json.date.toString(), rippleName: $rootScope.loggedInAccount.rippleName, rippleAddress: $rootScope.loggedInAccount.publicKey}} return item;});
									}
									if (!inList){
										transactionList.push({domain : $rootScope.domain, hash : res.tx_json.hash, dateTime: res.tx_json.date.toString(), rippleName: $rootScope.loggedInAccount.rippleName, rippleAddress: $rootScope.loggedInAccount.publicKey});
									}
									$localForage.setItem('lastTransactions', JSON.stringify(transactionList)).then(function() {

									});
								});
								poster(objectReq, defer);
							}
						});
					}
					if ($rootScope.remote.isConnected()){
						rippleFunc();
					}
					else {
						$rootScope.remote.connect(function() {
							rippleFunc();
						});
					}
				}
			});
		}
		if (!$rootScope.settings){
			accountFunctions.getRippleTxtSettings.then(function(){
				callTransact()
			});
		}
		else {
			callTransact()
		}
		return defer.promise;
	}
	function safe (val) {
		return ( typeof val !== "string"
			|| val.match(/[\r\n]/)
			|| val.match(/^\[/)
			|| (val.length > 1
			&& val.charAt(0) === "\""
			&& val.slice(-1) === "\"")
			|| val !== val.trim() )
			? JSON.stringify(val)
			: val.replace(/;/g, '\\;')
	}

	function unsafe (val, doUnesc) {
		val = (val || "").trim()
		if (val.charAt(0) === "\"" && val.slice(-1) === "\"") {
			try { val = JSON.parse(val) } catch (_) {}
		} else {
			var esc = false;
			var unesc = "";
			for (var i = 0, l = val.length; i < l; i++) {
				var c = val.charAt(i);
				if (esc) {
					if (c === "\\" || c === ";")
						unesc += c;
					else
						unesc += "\\" + c;
					esc = false;
				} else if (c === ";") {
					break;
				} else if (c === "\\") {
					esc = true;
				} else {
					unesc += c;
				}
			}
			if (esc)
				unesc += "\\";
			return unesc;
		}
		return val;
	}
	function dotSplit (str) {
		return str.replace(/\1/g, '\2LITERAL\\1LITERAL\2')
			.replace(/\\\./g, '\1')
			.split(/\./).map(function (part) {
				return part.replace(/\1/g, '\\.')
					.replace(/\2LITERAL\\1LITERAL\2/g, '\1')
			})
	}
	accountFunctions.decodered = function(str){
		var out = {}
			, p = out
			, section = null
			, state = "START"
			, re = /^\[([^\]]*)\]$|^([^=]+)(=(.*))?$/i
			, lines = str.split(/[\r\n]+/g)
			, section = null;

		lines.forEach(function (line, _, __) {
			if (!line || line.match(/^\s*;/)) return;
			var match = line.match(re);
			if (!match) return;
			if (match[1] !== undefined) {
				section = unsafe(match[1]);
				p = out[section] = out[section] || {};
				return;
			}
			var key = unsafe(match[2])
				, value = match[3] ? unsafe((match[4] || "")) : true;
			switch (value) {
				case 'true':
				case 'false':
				case 'null': value = JSON.parse(value)
			}
			if (key.length > 2 && key.slice(-2) === "[]") {
				key = key.substring(0, key.length - 2);
				if (!p[key]) {
					p[key] = [];
				}
				else if (!Array.isArray(p[key])) {
					p[key] = [p[key]];
				}
			}
			if (Array.isArray(p[key])) {
				p[key].push(value);
			}
			else {
				p[key] = value;
			}
		})
		Object.keys(out).filter(function (k, _, __) {
			if (!out[k] || typeof out[k] !== "object" || Array.isArray(out[k])) return false;
			var parts = dotSplit(k)
				, p = out
				, l = parts.pop()
				, nl = l.replace(/\\\./g, '.');
			parts.forEach(function (part, _, __) {
				if (!p[part] || typeof p[part] !== "object") p[part] = {}
				p = p[part];
			})
			if (p === out && nl === l) return false
			p[nl] = out[k];
			return true;
		}).forEach(function (del, _, __) {
				delete out[del];
			})
		return out;
	}
	accountFunctions.getInfo = function(){
		var options = {
			account: $rootScope.loggedInAccount.publicKey,
			ledger: 'validated'
		};
		var rippleFunc = function(){
			$rootScope.remote.requestAccountLines(options, function(err, info) {
				if (info && info.lines && info.lines[0]){
					info.lines.forEach(function(inf){
						if (inf.currency == 'PCV' && inf.issuer == $rootScope.pcvIssuer){
							$rootScope.balances.pcvAmount = inf.balance;
						}
						else if (inf.currency == 'PVD' && inf.issuer == $rootScope.receiveAddress){
							$rootScope.balances.pvdAmount = inf.balance;
						}
						else if (inf.currency == 'PRV' && inf.issuer == $rootScope.receiveAddress){
							$rootScope.balances.prvAmount = inf.balance;
						}
					});
				}
				if (!$rootScope.balances.prvAmount){
					$rootScope.balances.prvAmount = '0';
				}
				if (!$rootScope.balances.pvdAmount){
					$rootScope.balances.pvdAmount = '0';
				}
				if (!$rootScope.balances.pcvAmount){
					$rootScope.balances.pcvAmount = '0';
				}
				$rootScope.loggedInAccount.accountLines = info.lines;
				$rootScope.loggedInAccount.fountainLoaded = true;
				$rootScope.remote.requestAccountInfo(options, function(err, info) {
					if (info){
						$rootScope.loggedInAccount.accountInfo = info;
						if ($rootScope.loggedInAccount.accountInfo && $rootScope.loggedInAccount.accountInfo.account_data && $rootScope.loggedInAccount.accountInfo.account_data.Balance && !isNaN(Number($rootScope.loggedInAccount.accountInfo.account_data.Balance))){
							var request = $rootScope.remote.requestServerInfo();
							request.request(function(err, res) {
								if (res && res.info && res.info.validated_ledger && res.info.validated_ledger.reserve_base_xrp && res.info.validated_ledger.reserve_inc_xrp){
									var reserve = Number(res.info.validated_ledger.reserve_base_xrp) + (Number(res.info.validated_ledger.reserve_inc_xrp) * Number($rootScope.loggedInAccount.accountInfo.account_data.OwnerCount));
									$rootScope.loggedInAccount.reserve = reserve;
									if (Number($rootScope.loggedInAccount.accountInfo.account_data.Balance) > $rootScope.loggedInAccount.reserve){
										$rootScope.loggedInAccount.reserveMet = true;
									}
									else {
										$rootScope.loggedInAccount.reserveMet = false;
									}
								}
							});
						}
					}
					$rootScope.$apply();
				});
			});
		}
		if ($rootScope.remote.isConnected()){
			rippleFunc();
		}
		else {
			$rootScope.remote.connect(function() {
				rippleFunc();
			});
		}
	}
	accountFunctions.accountInfo = function(transaction){
		if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.publicKey && transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.loggedInAccount.publicKey || transaction.transaction.Account == $rootScope.loggedInAccount.publicKey){
			accountFunctions.getInfo();
		}
	}
	accountFunctions.messageInfo = function(transaction){
		if ($rootScope.loggedInAccount && transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && !isNaN(Number(transaction.transaction.Amount)) && transaction.transaction.Memos && transaction.transaction.Memos[0]){
			transaction.transaction.Memos.forEach(function(memo){
				if (memo.Memo){
					memo = memo.Memo;
				}
				if (memo.MemoType && ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'message'){
					var message = {};
					message.message = $sce.trustAsHtml($sanitize(linkify.twitterRippleProvidence(ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoData)))));
					message.date = transaction.transaction.date;
					message.hash = transaction.transaction.hash;
					message.address = transaction.transaction.Account;
					$http.get('https://id.ripple.com/v1/user/' + message.address).
						success(function(data, status, headers, config) {
							if (data && data.username){
								message.username = data.username;
							}
							var inRay = _.find($rootScope.chatData.messages, function(sigged) {return message.hash == sigged.hash;});
							if (!inRay){
								$rootScope.chatData.messages.unshift(message);
							}
						}).
						error(function(data, status, headers, config) {
							var inRay = _.find($rootScope.chatData.messages, function(sigged) {return message.hash == sigged.hash;});
							if (!inRay){
								$rootScope.chatData.messages.unshift(message);
							}
						});
				}
			});
		}
	}
	accountFunctions.sendTransaction = function(path, type, dt){
		var defer = $q.defer();
		var rippleFunc = function(){
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: ripple.Amount.from_json({"currency" : $rootScope.data.currency, "value" : $rootScope.data.amountCurrency.toString(), "issuer" : $rootScope.data.selectedGateway.rippleAddress})
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.addMemo('Escrow', JSON.stringify({domain : 'pier.exchange', service : 'Pier', type : type}));

			if (dt){
				transaction.destinationTag(Number(dt));
			}

			transaction.submit(function(err, res) {
				if (!err){
					defer.resolve(res.tx_json.hash);
				}
			});
		}
		if ($rootScope.remote.isConnected()){
			rippleFunc();
		}
		else {
			$rootScope.remote.connect(function() {
				rippleFunc();
			});
		}
		return defer;
	}
	accountFunctions.getRootTransactions = function(){
		var doneso = $q.defer();
		var count = 0;
		function getMessages(){
			var deferArray = [];
			var tad = $q.defer();
			count = 0;
			var options = {
				account: $rootScope.receiveAddress,
				ledger_index_min : -1,
				ledger_index_max : -1,
				offset: accountFunctions.offset,
				limit : 20,
				descending: true,
				forward : false,
				ledger: 'validated'
			};
			var rippleFunc = function(){
				$rootScope.remote.requestAccountTransactions(options, function(err,transactions){
					if (transactions && transactions.transactions && transactions.transactions[0]){
						function transactionYou(){
							var deferredPromises = [];
							var tadr = $q.defer();
							deferredPromises.push(tadr);
							count = transactions.transactions.length;
							transactions.transactions.forEach(function(tx){
								if (tx.tx.Memos && tx.tx.Memos[0]){
									tx.tx.Memos.forEach(function(memo){
										if (memo.Memo){
											memo = memo.Memo;
										}
										if (memo.MemoType && ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'message'){
											var message = {};
											message.message = $sce.trustAsHtml($sanitize(linkify.twitterRippleProvidence(ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoData)))));
											message.date = tx.tx.date;
											message.hash = tx.tx.hash;
											message.address = tx.tx.Account;
											var tadder = $q.defer();
											deferredPromises.push(tadder);
											$http.get('https://id.ripple.com/v1/user/' + message.address).
												success(function(data, status, headers, config) {
													if (data && data.username){
														message.username = data.username;
													}
													var inRay = _.find($rootScope.chatData.messages, function(sigged) {return message.hash == sigged.hash;});
													if (!inRay){
														$rootScope.chatData.messages.push(message);
													}
													tadder.resolve();
												}).
												error(function(data, status, headers, config) {
													var inRay = _.find($rootScope.chatData.messages, function(sigged) {return message.hash == sigged.hash;});
													if (!inRay){
														$rootScope.chatData.messages.push(message);
													}
													tadder.resolve();
												});
										}
									});
								}
							});
							tadr.resolve();
							return $q.all(deferredPromises);
						}
						transactionYou().then(function(){
							accountFunctions.offset += transactions.transactions.length;
							tad.resolve();
						});
					}
					else {
						tad.resolve();
					}
				});
			}
			if ($rootScope.remote.isConnected()){
				rippleFunc();
			}
			else {
				$rootScope.remote.connect(function() {
					rippleFunc();
				});
			}
			return tad.promise;
		}
		function messaged(){
			getMessages().then(function(){
				if (count >= 20 && $rootScope.chatData.messages.length < 10){
					messaged();
				}
				else {
					doneso.resolve('');
				}
			});
		}
		messaged();
		return doneso.promise;
	}
	accountFunctions.messageSubscribe = function(){
		var rippleFunc = function(){
			var request = $rootScope.globalRemote.requestSubscribe(['transactions']);
			request.setServer($rootScope.rippled);
			$rootScope.globalRemote.on('transaction', function onTransaction(transaction) {
				accountFunctions.accountInfo(transaction);
				accountFunctions.messageInfo(transaction);
			});
			request.request();
		}
		if ($rootScope.globalRemote.isConnected()){
			rippleFunc();
		}
		else {
			$rootScope.globalRemote.connect(function() {
				rippleFunc();
			});
		}
	}
	accountFunctions.makeImage = function(filed, card){
		if (filed.type.match(/image.*/)) {
			var img = document.createElement("img");
			var reader = new FileReader();
			reader.onload = function(e) {img.src = e.target.result}
			reader.readAsDataURL(filed);
			img.onload = function(){
				var canvas = document.createElement('canvas');
				var MAX_WIDTH = 200;
				var MAX_HEIGHT = 200;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				var dataurl = canvas.toDataURL("image/png");
				card.image = dataurl;
			}
		}
	}
	accountFunctions.validateSMS = function(number){
		var valid;
		function phonenumber(inputtxt){
			var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
			if ((inputtxt.match(phoneno))){
				return true;
			}
			else {
				return false;
			}
		}
		function internationalPhoneNumber(inputtxt){
			var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
			if((inputtxt.match(phoneno))){
				return true;
			}
			else {
				return false;
			}
		}
		if (number){
			var validInternational = internationalPhoneNumber(number);
			var validPhone = phonenumber(number);
			if (validInternational || validPhone){
				valid = true;
			}
		}
		return valid;
	}
	accountFunctions.validateEmail = function(email){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	accountFunctions.isValidUrl = function(url){
		return url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
	}
	return accountFunctions;
}])

.factory('socialFunctions', ['$rootScope', '$http', '$q', '$sce', '$localForage', function($rootScope, $http, $q, $sce, $localForage) {
	var socialFunctions = {};
	socialFunctions.generateNeumonic = function(words){
		var from = 96;
		if (words){
			var oldWords = Mnemonic.words;
			from = words.split(' ');
			if (from && from[0]){
				from.forEach(function(fr){
					if (!$.inArray(fr, Mnemonic.words)){
						Mnemonic.words.push(fr);
					}
				});
			}
		}
		var m = new Mnemonic(from);

		var returnPacket = {words: m.toWords(), hex: m.toHex()};

		if (!words){
			var counter = 1;
			returnPacket.words.forEach(function(word){
				if (counter != returnPacket.words.length){
					returnPacket.formattedWords += word + ' ';
				}
				else {
					returnPacket.formattedWords += word;
				}
				counter += 1;
			});
		}
		else {
			returnPacket.formattedWords = words;
			Mnemonic.words = oldWords;
		}
		return returnPacket;
	}
	return socialFunctions;
}]);