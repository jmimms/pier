module.exports = function(grunt) {

	grunt.loadNpmTasks('grunt-bower-install');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-closurecompiler');
	grunt.loadNpmTasks('grunt-angular-templates');

	grunt.initConfig({
		cssmin: {
			combine: {
				files: {
					'www/css/vendor.css' : [
						'www/css/ionic.min.css',
						'www/css/stylesheet.css',
						'www/lib/ng-tags-input/ng-tags-input.css',
						'www/css/style.css'
					]
				}
			}
		},
		concat: {
			dist: {
				src: [
					'www/lib/jquery/dist/jquery.min.js',
					'www/lib/ionic/js/ionic.bundle.js'
				],
				dest: 'www/js/vendor1.js'
			},
			dist2: {
				src: [
					'www/lib/lodash/dist/lodash.compat.js',
					'www/lib/moment/min/moment.min.js',
					'www/lib/angular-moment/angular-moment.min.js',
					'www/lib/ripple/ripple-min.js',
					'www/lib/angular-gravatar/build/md5.min.js',
					'www/lib/angular-gravatar/build/angular-gravatar.min.js',
					'www/lib/angular-lodash/angular-lodash.js',
					'www/lib/chance/chance.js',
					'www/lib/fast-sha256/sha256.min.js',
					'www/lib/angular-emoticons/javascripts/angular-emoticons.js',
					'www/lib/localforage/dist/localforage.js',
					'www/lib/angular-localforage/dist/angular-localForage.js',
					'www/lib/jquery-waypoints/waypoints.min.js',
					'www/lib/angular-waypoints/dist/angular-waypoints.min.js',
					'www/lib/swipecards/ionic.swipecards.js',
					'www/lib/angular-timer/dist/angular-timer.js',
					'www/lib/signature_pad/signature_pad.js',
					'http://platform.twitter.com/widgets.js',
					'www/lib/angular-route/angular-route.min.js',
					'www/lib/angular-linkify/angular-linkify.js',
					'www/lib/ng-tags-input/ng-tags-input.js',
					'www/lib/mnemonic/mnemonic.js',
					'www/lib/lunr/lunr.min.js',
					'www/js/app.js',
					'www/js/controllers.js',
					'www/js/services.js'
				],
				dest: 'www/js/vendor2.js'
			}
		},
		closurecompiler: {
			application: {
				files: {
					'www/js/vendor1.min.js' : [
						'www/js/vendor1.js'
					]
				},
				options: {
					"compilation_level": "SIMPLE_OPTIMIZATIONS",
					"max_processes": 5
				}
			},
			application2: {
				files: {
					'www/js/vendor2.min.js' : [
						'www/js/vendor2.js'
					]
				},
				options: {
					"compilation_level": "SIMPLE_OPTIMIZATIONS",
					"max_processes": 5
				}
			}
		},
		ngtemplates: {
			starter:          {
				cwd:		'www',
				src:        'templates/**.html',
				dest:       'js/templates.js',
				options:    {
					htmlmin: {
						collapseBooleanAttributes:      true,
						collapseWhitespace:             true,
						removeAttributeQuotes:          true,
						removeComments:                 true,
						removeEmptyAttributes:          true,
						removeRedundantAttributes:      true,
						removeScriptTypeAttributes:     true,
						removeStyleLinkTypeAttributes:  true
					}
				}
			}
		},
		bowerInstall: {
			target: {
				src: 'www/index.html',
				cwd: '',
				dependencies: true,
				devDependencies: false,
				exclude: [],
				fileTypes: {},
				ignorePath: '',
				overrides: {}
			}
		}
	});

	grunt.registerTask('default', ['ngtemplates', 'concat', 'cssmin']);
	grunt.registerTask('bower', ['bowerInstall']);

};